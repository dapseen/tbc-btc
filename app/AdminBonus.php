<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminBonus extends Model
{
    protected $fillable = 
	[
        'level_1_amount', 'level_2_amount',
    ];
	
	public static function setupRates($rates)
	{
		static::truncate();
		
		static::create($rates);
	}
	
	public static function getLevel1BonusRate()
	{
		return static::firstOrCreate(array())->level_1_amount;
	}
	
	public static function getLevel2BonusRate()
	{
		return static::firstOrCreate(array())->level_2_amount;
	}
}