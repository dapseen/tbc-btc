<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrencyRates extends Model
{
    protected $fillable = 
	[
        'BTC_TO_USD', 'ETH_TO_USD', 'LTC_TO_USD', 'USD_TO_NGN',
    ];
	
	public static function setupRates($rates)
	{
		static::truncate();
		
		static::create($rates);
	}
	
	public static function getBTCUSDValue()
	{
		return static::firstOrCreate(array())->BTC_TO_USD;
	}
	
	public static function getETHUSDValue()
	{
		return static::firstOrCreate(array())->ETH_TO_USD;
	}
	
	public static function getLTCUSDValue()
	{
		return static::firstOrCreate(array())->LTC_TO_USD;
	}
	
	public static function getUSDNGNValue()
	{
		return static::firstOrCreate(array())->USD_TO_NGN;
	}
}