<?php

namespace App\Http\Controllers;

use App\AdminBonus;
use Illuminate\Http\Request;

class AdminBonusController extends Controller
{

/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  //manage all rate page

	public function index()
	{
	  return view('admin_bonus.manage_bonus');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin_bonus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //write a function to only create bonus once and redirect if bonus is available

	AdminBonus::setupRates($request->all());


return redirect()->back();
	
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdminBonus  $adminBonus
     * @return \Illuminate\Http\Response
     */
    public function show(AdminBonus $adminBonus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdminBonus  $adminBonus
     * @return \Illuminate\Http\Response
     */
    public function edit(AdminBonus $adminBonus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdminBonus  $adminBonus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdminBonus $adminBonus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdminBonus  $adminBonus
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdminBonus $adminBonus)
    {
        //
    }
}
