<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
	 
    public function index()
    {
		$user = \Auth::user();

	    if(! ($user->is_admin && $user->user_manager) )
		{
			return redirect('/home');//->with('status', 'You do not have the required permissions to create accounts.');
		}
		   
		$users = DB::select('select * from users where is_admin != 1');
		
		$admins = DB::select('select * from users where is_admin = 1');
		
		return view('admin', ['users'=>$users], ['admins'=>$admins]);
    }
}
