<?php

namespace App\Http\Controllers;



use App\RetonarMail;
use Illuminate\Http\Request;
use App\User;
use App\Investment;
use App\Transaction;

class Agent extends Controller
{
    public function payToAgent(Request $request){
	
		$data = $request->all();

		//pay to agent class will get request from form and send to email and save some in database
		if(!empty($request))
			{

				/**
				 * validate data
				 * 
				 

				*$min = Investment::getMinMaxInvestmentAmounts($request['plan'])[0];
		
	*			$max = Investment::getMinMaxInvestmentAmounts($request['plan'])[1];

	*			 $this->validate($request,[
	*					'amount'=>'required|numeric|between:100,1000'
				* ]);
				 */

				$user = \Auth::user();
				
				$amount = floatval( $request->input('amount') );
				$amount -= ( Investment::DEPOSIT_FEE * $amount );
				
					$investment = Investment::create([
						'investment_user_id' => $user->id,
						'investment_amount' => $amount,
						'investment_type' => $request->input('plan'),
						'investment_status' => 'unconfirmed',
						'investment_transaction_id' => 'still unknown',
					]);
					
					$transaction = Transaction::create([
						'transaction_user_id' => $user->id,
						'transaction_amount' => $amount,
						'transaction_type' => 'deposit',
						'transaction_status' => $investment->investment_status,
						'transaction_investment_id' => $investment->investment_id,
					]);
					
					$investment->investment_transaction_id = $transaction->transaction_id;
					
					$investment->save();
		
				RetonarMail::sendAgentForm('retonarbox@gmail.com','New Agent Form','A user just submitted a form for Agent',$data);
			}
		return redirect('/home')->with('status', 'We have notified an agent in your area, we will get back to you shortly');
		

	}


}
