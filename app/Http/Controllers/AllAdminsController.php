<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AllAdminsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     
    public function index()
    {
        $user = \Auth::user();

        $admins = DB::select('select * from users where is_admin = 1');
        
        return view('all_admins', ['admins'=>$admins]);
    }
}
