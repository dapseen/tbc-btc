<?php

namespace App\Http\Controllers;

use App\Investment;
use App\Transaction;
use Illuminate\Http\Request;
use App\User;
use App\RetonarMail;
use DB;

class AllInvestmentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     
    public function index()
    {
        $user = \Auth::user();
        
	    if(! ($user->is_admin && $user->user_manager) )
		{
			return redirect('/admin')->with('status', 'You do not have the required permissions to view investments.');
		}
		  
		$investments = Investment::all()->reverse();
			
		$usernames = array();
		
		foreach($investments as $investment)
		{
			$user = DB::select("select * from users where id = $investment->investment_user_id");
			
			$usernames[$investment->investment_user_id] = !empty($user) ? $user[0]->name : "unknown";
		}
		
		return view('all_investments', ['investments'=>$investments, 'usernames'=>$usernames]);
	}
	
    public function activateInvestment($investmentID)
    {
        $user = \Auth::user();
        
	    if(! ($user->is_admin && $user->user_manager) )
		{
			return redirect('/admin')->with('status', 'You do not have the required permissions to activate investments.');
		}
		  
		$investment = Investment::where('investment_id', $investmentID)->firstOrFail();
		
		if($investment->investment_status == "unconfirmed")
		{
			$transaction = Transaction::where('transaction_investment_id', $investmentID)->firstOrFail();
			
			$transaction->transaction_status = "confirmed";
			
			$transaction->save();
		}
		
		$investment->investment_status = "active";
		
		$investment->save();
		
		$investment->created_at = $investment->updated_at;
		
		$investment->save();
		
		RetonarMail::send(User::find($investment->investment_user_id)->email, "Investment Activated", "Your new investment has been activated.");
		
		return redirect()->back()->with("status", "Investment successfully activated.");
	}
	
    public function deactivateInvestment($investmentID)
    {
        $user = \Auth::user();
        
	    if(! ($user->is_admin && $user->user_manager) )
		{
			return redirect('/admin')->with('status', 'You do not have the required permissions to deactivate investments.');
		}
		  
		$investment = Investment::where('investment_id', $investmentID)->firstOrFail();
		
		$investment->investment_status = "inactive";
		
		$investment->save();
		
		return redirect()->back()->with("status", "Investment successfully deactivated.");
	}
}
