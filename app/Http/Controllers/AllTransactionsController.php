<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AllTransactionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = \Auth::user();
        
	    if(! ($user->is_admin && $user->user_manager) )
		{
			return redirect('/admin')->with('status', 'You do not have the required permissions to view transactions.');
		}
		  
		$transactions = DB::select("select * from transactions");
			
		arsort($transactions);
			
		foreach($transactions as $transaction)
		{
			$user = DB::select("select * from users where id = $transaction->transaction_user_id");
			
			$usernames[$transaction->transaction_user_id] = !empty($user) ? $user[0]->name : "unknown";
		}
		
		return view('all_transactions', ['transactions'=>$transactions, 'usernames'=>$usernames]);
    }
}