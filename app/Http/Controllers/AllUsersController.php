<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Investment;
use DB;

class AllUsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     
    public function index()
    {
		$user = \Auth::user();
	   
	    if(! ($user->is_admin && $user->user_manager) )
		{
			return redirect('/admin')->with('status', 'You do not have the required permissions to view user accounts.');
		}
		   
        $users = DB::select('select id,name,email,first_name,last_name,bank_account_name,account_number,referral_id 
        FROM users where is_admin != 1' );
       
        
        return view('all_users', ['users'=>$users]);
    }

    public function individual_user($id)
	  {
		     $user = User::find($id);
		
		     $investments = Investment::where("investment_user_id", $id)->get();
		
        

        return view('individual_users',['user'=>$user, 'investments'=>$investments]);
    }


}
