<?php

namespace App\Http\Controllers;

use App\WithdrawalRequest;
use App\Investment;
use App\Transaction;
use App\User;
use App\RetonarMail;
use Illuminate\Http\Request;
use DB;

class AllWithdrawalRequestsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     
    public function index()
    {
        $user = \Auth::user();
        
	    if(! ($user->is_admin && $user->user_manager) )
		{
			return redirect('/admin')->with('status', 'You do not have the required permissions to view withdrawal requests.');
		}
		  
		$withdrawalRequests = WithdrawalRequest::where('request_status', '!=', 'accepted')->where('request_status', '!=', 'rejected')->latest()->get();
			
		$usernames = array();
		
		foreach($withdrawalRequests as $withRequest)
		{
			$user = User::where("id", $withRequest->from_user_id)->first();
			
			$usernames[$withRequest->from_user_id] = !empty($user) ? $user->name : "unknown";
		}
		
		return view('all_withdrawal_requests', ['requests'=>$withdrawalRequests, 'usernames'=>$usernames]);
	}
	
    public function acceptRequest($requestID)
    {
        $cuser = \Auth::user();
        
	    if(! ($cuser->is_admin && $cuser->user_manager) )
		{
			return redirect('/admin')->with('status', 'You do not have the required permissions to accept withdrawal requests.');
		}
		  
		$wrequest = withdrawalRequest::find($requestID);
		
		$owner = User::find($wrequest->from_user_id);
			
		$wrequest->request_status = "accepted";
		
		$wrequest->save();
		
		{
      $transaction = Transaction::create([
      'transaction_user_id' => $owner->id,
      'transaction_amount' => $wrequest->requested_amount,
			'transaction_type' => 'withdrawal',
			'transaction_status' => 'confirmed',
			'transaction_investment_id' => $wrequest->from_investment_id,
          ]);
		}
		
		RetonarMail::send($owner->email, "Withdrawal accepted", "Your withdrawal request has been accepted."); 		
		
		return redirect()->back()->with("status", "Request successfully accepted.");
	}
	
    public function rejectRequest($requestID)
    {
        $user = \Auth::user();
        
	    if(! ($user->is_admin && $user->user_manager) )
		{
			return redirect('/admin')->with('status', 'You do not have the required permissions to reject withdrawal requests.');
		}
		  
		$wrequest = withdrawalRequest::find($requestID);
		
		$wrequest->request_status = "rejected";
		
		$wrequest->save();
		
		$owner = User::find($wrequest->from_user_id);
		
		
		
		//REVERSE DEBIT
		$toBeActuallyDebited = floatval( $wrequest->requested_amount ) + ( Investment::WITHDRAWAL_FEE * $wrequest->requested_amount );
		
		if($wrequest->from_investment_id == "none")
		{
			ReferralController::referralDebit($owner, -$toBeActuallyDebited);
		}
		else
		{
			$investment = Investment::find($wrequest->from_investment_id);
			
			  $investment->investment_total_interest_debit = (-$toBeActuallyDebited) + floatval( $investment->investment_total_interest_debit );
		
		    $investment->save();
    }
		
		
		
		RetonarMail::send($owner->email, "Withdrawal rejected", "Your withdrawal request has been rejected."); 		
		
		return redirect()->back()->with("status", "Request successfully rejected.");
	}
}
