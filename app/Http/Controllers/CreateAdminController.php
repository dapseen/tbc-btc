<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use App\Mail\VerifyMail;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class CreateAdminController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/admin/create';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Create a new user admin
     */
    protected function index()
    {
		$user = \Auth::user();
	   
	    if(! ($user->is_admin && $user->user_manager) )
		{
			return redirect('/admin')->with('status', 'You do not have the required permissions to create accounts.');
		}
		   
		return view("create_admin");
    }
	
	
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
		Validator::extend('no_space', function($attr, $value){
			return preg_match('/^\S*$/u', $value);
		});
			
        return Validator::make($data, [
            'name' => 'required|no_space|unique:users|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
			'first_name' => "_",
			'last_name' => "_",
			'bank_account_name' => "_",
			'account_number' => "_",
			'referral_id' => "",
			'investment_plan' => "_",
            'password' => Hash::make($data['password']),
        ]);
		
		if(!empty($data['user_manager']) && $data['user_manager'] == 'true')
		{
			$user->user_manager = 1;
		}
		 
		 
		if(!empty($data['withdrawal_manager']) && $data['withdrawal_manager'] == 'true')
		{
			$user->withdrawal_manager = 1;
		}
		
		$user->verified = 1;
		$user->is_admin = 1;
		$user->save();
		  
		return \Auth::user();
    }
	
	protected function registered(Request $request, $user)
    {
        return redirect()->back()->with('status', 'Admin created successfully.');
    }
}
