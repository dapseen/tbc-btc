<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use App\Mail\VerifyMail;
use App\User;
use App\Investment;
use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\CoinPaymentsAPI;

class CreateInvestmentController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('auth');
    }

	protected function displayPaymentMethods($plan)
    {
		$user = \Auth::user();
		if (count(Investment::where('investment_user_id','=',$user->id)->get() ) > 0 ){
			return redirect('/home')->with('status', 'You can only have one investment plan at a time.');
		}

	    if(!isset(Investment::ALL_INVESTMENT_PLANS[$plan]))
		{
			return redirect('/home')->with('status', 'You selected an invalid investment plan.');
		}
		
		return view("ci_display_payment_methods", ['plan'=>$plan]);
    }
	
	private function displaySendToAddress(Request $request)
	{
		
		$plan = $request->input("plan");
		
		$admin_fee = Investment::ALL_INVESTMENT_PLANS[$plan]['admin_fee'];
		

		$amount = $request->input('amount') + $admin_fee;

		
		$crypto_sign = $request->input('currency2');
		
		$crypto_amount = $request->input( strtolower($crypto_sign)."-amount" );
		
		$wallet_details = NULL;
		
	    $cps = new CoinPaymentsAPI();
	    
		$cps->Setup();

	    $req = 
		array(
		  'amount' => $amount,
		  'currency1' => 'USD',
		  'currency2' => $crypto_sign,
		  'address' => '',
		  'custom' => strtoupper( $plan . " investment plan by " . \Auth::user()->name ),
		);
	
	    $result = $cps->CreateTransaction($req);
	    
		if ($result['error'] == 'ok') 
		{
		    $wallet_details = array($result['result']['address'], $result['result']['qrcode_url']);
	    } 
		else 
		{
		    die( 'Error: '.$result['error']."\n" );
		}
		
		return view("ci_display_sendto_address", ['admin_fee'=>$admin_fee, 'plan'=>$plan, 'amount'=>$amount, 'crypto_sign'=>$crypto_sign, 'crypto_amount'=>$crypto_amount, "wallet_details"=>$wallet_details]);
	}
	
    protected function validator(array $data)
    {
		$min = Investment::getMinMaxInvestmentAmounts($data['plan'])[0];
		
		$max = Investment::getMinMaxInvestmentAmounts($data['plan'])[1];
		
		Validator::extend('no_space', function($attr, $value){
			return preg_match('/^\S*$/u', $value);
		});
			
        return Validator::make($data, [
            'amount' => "required|numeric|between:$min,$max",
        ]);
    }
	
    protected function create(array $data)
    {
		return \Auth::user();
    }
	
    protected function createInvestment(Request $request)
    {
		$user = \Auth::user();
	   
	   $amount = floatval( $request->input('amount') );
	   $amount -= ( Investment::DEPOSIT_FEE * $amount );
	   
        $investment = Investment::create([
            'investment_user_id' => $user->id,
            'investment_amount' => $amount,
			'investment_type' => $request->input('plan'),
			'investment_status' => 'unconfirmed',
			'investment_transaction_id' => 'still unknown',
        ]);
		
        $transaction = Transaction::create([
            'transaction_user_id' => $user->id,
            'transaction_amount' => $amount,
			'transaction_type' => 'deposit',
			'transaction_status' => $investment->investment_status,
			'transaction_investment_id' => $investment->investment_id,
        ]);
		
		$investment->investment_transaction_id = $transaction->transaction_id;
		
		$investment->save();
    }
	
	protected function registered(Request $request, $user)
    {
		if( !empty($request->input('sendto')) )
		{
			return $this->displaySendToAddress($request);
		}
		else
        {
			$this->createInvestment($request);
			
			return redirect('/home')->with('status', 'Investment created successfully. Admin will now verify your investment.');
		}
    }
}
