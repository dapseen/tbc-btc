<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use App\Mail\VerifyMail;
use App\CurrencyRates;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class CurrencyRatesController extends Controller
{
    use RegistersUsers;

    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function index()
    {
		$user = \Auth::user();
	   
	    if(! ($user->is_admin) )
		{
			return redirect('/admin')->with('status', 'You do not have the required permissions to view this page.');
		}
		   
		return view("admin_layout/currency_rates");
    }
	
	
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
		Validator::extend('no_space', function($attr, $value){
			return preg_match('/^\S*$/u', $value);
		});
			
        return Validator::make($data, [
            /*'x' => 'required|no_space|unique:users|string|max:255',
            'y' => 'required|string|email|max:255|unique:users',
            'z' => 'required|string|min:6|confirmed',*/
        ]);
    }

    protected function create(array $data)
    {
        CurrencyRates::setupRates($data);
		  
		return \Auth::user();
    }
	
	protected function registered(Request $request, $user)
    {
        return redirect()->back()->with('status', 'Rates modified successfully.');
    }
}
