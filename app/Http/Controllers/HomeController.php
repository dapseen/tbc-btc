<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Transaction;
use App\Investment;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
	 
    public function index()
    {
		     $user = \Auth::user();

		     if($user->is_admin)
			   {
		    	   return redirect('/admin');
        }
		    else
	    	 {
			      $my_transactions = DB::select("select * from transactions where transaction_user_id = $user->id");
			
			      arsort($my_transactions);
			
			
			
			      $my_investments = Investment::where("investment_user_id", $user->id)->get()->all();
			
			      arsort($my_investments);
			
			
			
			      return view('home', ['my_investments'=>$my_investments, 'my_transactions'=>$my_transactions]);
		    }
  }
}
