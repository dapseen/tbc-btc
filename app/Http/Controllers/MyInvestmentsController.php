<?php

namespace App\Http\Controllers;

use App\Investment;
use Illuminate\Http\Request;
use DB;

class MyInvestmentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     
    public function index()
    {
		$user = \Auth::user();
	   
		$my_investments = 
		    Investment::where('investment_user_id', $user->id)
		    ->where(function ($query) 
		{ 
		   //$query->where('investment_status', '=', 'expired');
		   //$query->where('investment_balance', '<', 0); 
		}) ->get();
		
		return view('my_investments', ['my_investments'=>$my_investments]);
    }

    
}
