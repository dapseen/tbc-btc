<?php

namespace App\Http\Controllers;

use App\WithdrawalRequest;
use App\Investment;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use DB;

class MyWithdrawalRequestsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     
    public function index()
    {
        $user = \Auth::user();
		  
		$withdrawalRequests = WithdrawalRequest::where('from_user_id', $user->id)->latest()->get();
		
		return view('with_request.show_my_withdrawal_requests', ['requests'=>$withdrawalRequests]);
	}
}