<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use \App\User;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
     
    public function index()
    {
       
        $user = \Auth::user();
        
        return view('profile');
    }
    
    public function edit($id)
    {
       
        $user = \Auth::user($id);
       
        
        return view('users.profile_update')->with(['user'=>$user]);
    }
    
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        
	    /*$rules = [
             'password'   => 'required|numeric',
             'password' => 'required'
        ];
  
        $validator = Validator::make($request->all(), $rules);
		
		if($validator->fails())
		{
			return view('users.profile_update')->withErrors($validator);
		}*/
		
       
        
        $user->update($request->all());
        
        return redirect()->back()->with("status", " profile Updated successfully.");
    }

	public function buyCrypto(){
	
	return view('users.buyCrypto');
	
	}
}
