<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use App\Mail\VerifyMail;
use App\User;
use App\Investment;
use App\Transaction;
use App\WithdrawalRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RefWithdrawalController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('auth');
    }
     
    public function index()
    {
		$user = \Auth::user();
		
		return view('with_request.create', ['investment_id'=>"none"]);
    }
	
	
	
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
		$user = \Auth::user();
	   
		$min = 20;
		//$max = 0;
        return Validator::make($data, [
            'requested_amount' => "required|numeric|min:$min",
            'withdrawal_method' => "required|string",
        ]);
    }
	
    protected function create(array $data)
    {
	    	return \Auth::user();
    }
	
	protected function registered(Request $request, $user)
    {
		$user = \Auth::user();
	   
		$current_balance = ReferralController::getReferralCurrentTotalBonus ($user);
		
		$amount_requested = floatval( $request->input('requested_amount') );
			
		if( $current_balance < $amount_requested + ( Investment::WITHDRAWAL_FEE * $amount_requested ) )
		{
			return redirect()->back()->with('status', 'Sorry. Your balance is too low for the specified operation.');
		}
		
		$data = $request->all();
		
		$data['from_user_id'] = $user->id;
			
        $withdrawalRequest = WithdrawalRequest::create($data);
		



//DEBIT
		$toBeActuallyDebited = floatval( $withdrawalRequest->requested_amount ) + ( Investment::WITHDRAWAL_FEE * $withdrawalRequest->requested_amount );
		
		{
			ReferralController::referralDebit($user, $toBeActuallyDebited);
		}



        return redirect('/home')->with('status', 'Withdrawal Request created successfully. Admin will process your request.');
    }
}
