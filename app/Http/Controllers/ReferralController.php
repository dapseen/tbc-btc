<?php

namespace App\Http\Controllers;

use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Investment;
use App\Transaction;
use App\User;
use App\AdminBonus;
use DB;

class ReferralController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function my_referrals()
    {
        $user_auth = \Auth::user();
		
        $level1Referrals = self::getLevel1Referrals($user_auth);

        $level2Referrals = self::getLevel2Referrals($level1Referrals);

        $eachUserInvested = array();

        $bonusOnEachUser = array();

	      foreach ($level1Referrals as $ref)
        {
           $id = $ref->id;
	
			      $eachUserInvested[$id] =
			          Transaction::where('transaction_user_id', $id)
			          ->where('transaction_status', 'confirmed')
			          ->where('transaction_type', 'deposit')
			          ->sum('transaction_amount');
			
		        $bonusOnEachUser[$id] = (AdminBonus::getLevel1BonusRate()/100) * $eachUserInvested[$id];
			   }

	        foreach ($level2Referrals as $ref)
            {
                  $id = $ref->id;
	
			      $eachUserInvested[$id] =
			          Transaction::where('transaction_user_id', $id)
			          ->where('transaction_status', 'confirmed')
			          ->where('transaction_type', 'deposit')
			          ->sum('transaction_amount');
		
		        $bonusOnEachUser[$id] = (AdminBonus::getLevel2BonusRate()/100) * $eachUserInvested[$id];
			}

			$totalBonus = self::getReferralTotalBonusFromHistory ($user_auth);
		
		    $totalDebited = self::getReferralTotalDebited($user_auth);
			
        return view('referral.my_referrals', ['level1Referrals'=>$level1Referrals, 'level2Referrals'=>$level2Referrals, 'eachUserInvested'=>$eachUserInvested, 'bonusOnEachUser'=>$bonusOnEachUser, 'totalBonus'=>$totalBonus, 'totalDebited'=>$totalDebited]);
    }

    public static function getReferralCurrentTotalBonus( $user_auth )
    {
		return self::getReferralTotalBonusFromHistory($user_auth) - $user_auth->ref_debited;
	}
	
    public static function getReferralTotalBonusFromHistory( $user_auth )
    {
        $level1Referrals = self::getLevel1Referrals($user_auth);

        $level2Referrals = self::getLevel2Referrals($level1Referrals);

        $eachUserInvested = array();

        $bonusOnEachUser = array();

	      foreach ($level1Referrals as $ref)
        {
           $id = $ref->id;
	
			      $eachUserInvested[$id] =
			          Transaction::where('transaction_user_id', $id)
			          ->where('transaction_status', 'confirmed')
			          ->where('transaction_type', 'deposit')
			          ->sum('transaction_amount');
			
		        $bonusOnEachUser[$id] = (AdminBonus::getLevel1BonusRate()/100) * $eachUserInvested[$id];
			   }

	        foreach ($level2Referrals as $ref)
            {
                  $id = $ref->id;
	
			      $eachUserInvested[$id] =
			          Transaction::where('transaction_user_id', $id)
			          ->where('transaction_status', 'confirmed')
			          ->where('transaction_type', 'deposit')
			          ->sum('transaction_amount');
		
		        $bonusOnEachUser[$id] = (AdminBonus::getLevel2BonusRate()/100) * $eachUserInvested[$id];
			}

			$totalBonus = array_sum($bonusOnEachUser);
		
		    $totalDebited = self::getReferralTotalDebited($user_auth);
			
        return $totalBonus;
    }

    public static function getLevel1Referrals($fromUser)
    {
        return User::where('referral_id', $fromUser->name)->get();
    }

    public static function getLevel2Referrals($level1Referrals)
    {
        $level2Referrals = new Collection([]);

        foreach($level1Referrals as $user)
        {
	          $refsOfUser = self::getLevel1Referrals($user);
	
	          $level2Referrals = $level2Referrals->merge($refsOfUser);
        }

        return $level2Referrals ;
    }
	
    public static function getReferralTotalDebited($fromUser)
    {
        return $fromUser->ref_debited;
    }

    public static function referralDebit($fromUser, $amount)
    {
		$fromUser->ref_debited += $amount;
		$fromUser->save();
    }
}