<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use App\Mail\VerifyMail;
use App\User;
use App\Transaction;
use App\Investment;
use App\AdminBonus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class SetupController extends Controller
{
    protected function index()
    {
       self::defaultAdmin();
		
       $toppers = self::getLeaderBoard(5);

	   return view("index", ["toppers"=>$toppers]);
    }




public function __construct()
{   
    //all bad codes ---- temporary use only
		//DB::delete('delete from investments where investment_id = ?',[14]);
		//DB::delete('delete from investments where investment_id = ?',[15]);
		//$investments = DB::table('investments')->update(array( 'investment_status'=>'active', ));
		//$investments = DB::table('transactions')->update(array( 'transaction_status'=>'confirmed', ));
		//DB::table('investments')->where('investment_id', $id)->update(['investment_balance' => strval(750)]);
		//DB::table('investments')->where('investment_id', $id)->update(['investment_amount' => strval(23000)]);
		//$investment = Investment::where('investment_id', $investmentID)->firstOrFail();
   //$u = User::where('email', "retribution@retribution.org")->get()->first();
   //$u->referral_id = "QnS";
   //$u->save();
}



  public static function getLeaderBoard($count)
  {
	   $toppers = array();
	
     foreach( (User::all()) as $user)
     {
       $moni = self::getTotalRefBonus($user);

       if($moni > 0)
	     {
		       $toppers[$user->name] = $moni;
	     }
     }

    arsort($toppers);

    return $toppers;
  }

  private static function getTotalRefBonus($user)
  {
        $level1Referrals = ReferralController::getLevel1Referrals($user);

        $level2Referrals = ReferralController::getLevel2Referrals($level1Referrals);

        $eachUserInvested = array();

        $bonusOnEachUser = array();

	      foreach ($level1Referrals as $ref)
        {
           $id = $ref->id;
	
			      $eachUserInvested[$id] =
			          Transaction::where('transaction_user_id', $id)
			          ->where('transaction_status', 'confirmed')
			          ->sum('transaction_amount');
			
		        $bonusOnEachUser[$id] = (AdminBonus::getLevel1BonusRate()/100) * $eachUserInvested[$id];
			   }

	      foreach ($level2Referrals as $ref)
        {
           $id = $ref->id;
	
			      $eachUserInvested[$id] =
			          Transaction::where('transaction_user_id', $id)
			          ->where('transaction_status', 'confirmed')
			          ->sum('transaction_amount');
		
		        $bonusOnEachUser[$id] = (AdminBonus::getLevel2BonusRate()/100) * $eachUserInvested[$id];
			   }
			
			return array_sum($bonusOnEachUser);
  }

  private static function defaultAdmin()
  {
		$admins = User::where('is_admin', '1')->first();
		
		if(empty($admins)){
			
			$user = User::create([
            'name' => "admin",
            'email' => "admin@admin.com",
			'first_name' => "_",
			'last_name' => "_",
			'bank_account_name' => "_",
			'account_number' => "_",
			'investment_plan' => "_",
			'referral_id' => "",
            'password' => Hash::make("admin"),
          ]);
		  
		  $user->verified = 1;
		  $user->is_admin = 1;
		  $user->user_manager = 1;
		  $user->withdrawal_manager = 1;		  
		  $user->save();
		}
  }
}




