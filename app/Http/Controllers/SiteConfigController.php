<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\siteConfig;

class SiteConfigController extends Controller
{
    public function index ()
    {

        return view('site_config.site_config');
    }

    //create configuration

    public function create(Request $request)
    {
       
        $siteConfig = new siteConfig;
        $siteConfig->site_title = $request->site_title;
        $siteConfig->site_color = $request->site_color;
        $siteConfig->site_description = $request->site_description;

        dd($siteConfig->site_title);
    }

    

    //update site configuration

    public function update(Request $request)
    {

    }
}
