<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class TransactionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

   
    
     
     
    public function index()
    {
        $user = \Auth::user();
        
		$my_transactions = DB::select("select * from transactions where transaction_user_id = $user->id");
			
		arsort($my_transactions);
			
		return view('transactions', ['my_transactions'=>$my_transactions]);
    }
}
