<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserDeleteController extends Controller {
   public function index(){
      $users = DB::select('select * from users');
      return view('user_delete_view',['users'=>$users]);
   }
   
   public function destroy($id) {
	   $user = \Auth::user();
	   
	   if($user->is_admin && $user->user_manager)
	   {
		DB::delete('delete from users where id = ?',[$id]);
        //echo "Record deleted successfully.<br/>";
        //echo '<a href="/delete-records">Click Here</a> to go back.';
	    return redirect()->back()->with('status', 'Account deleted successfully.');
	  }
	  else
	  {
		return redirect()->back()->with('status', 'You do not have the required permissions to delete accounts.');
	  }
   }
}