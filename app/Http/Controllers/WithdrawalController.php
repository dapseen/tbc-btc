<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use App\Mail\VerifyMail;
use App\User;
use App\Investment;
use App\Transaction;
use App\WithdrawalRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class WithdrawalController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('auth');
    }
     
    public function index($id)
    {
		    $user = \Auth::user();
		
		   return view('with_request.create', ['investment_id'=>$id]);
    }
	
	
	
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
		$user = \Auth::user();
	   
	    $i_id = $data['from_investment_id'];
	   
		$investment = Investment::where('investment_user_id', $user->id)->where('investment_id', $i_id)->first();
		
		$min = Investment::getMinMaxWithdrawalAmounts($investment->investment_type)[0];
		
		//$max = Investment::getMinMaxWithdrawalAmounts($investment->investment_type)[1];
	
        return Validator::make($data, [
            'requested_amount' => "required|numeric|min:$min",
            'withdrawal_method' => "required|string",
        ]);
    }
	
    protected function create(array $data)
    {
		return \Auth::user();
    }
	
	protected function registered(Request $request, $user)
    {
		$user = \Auth::user();
	   
	    $i_id = $request->input('from_investment_id');
	   
		$investment = Investment::where('investment_user_id', $user->id)->where('investment_id', $i_id)->first();
		
		$current_balance = floatval($investment->getBalance());
			
		$amount_requested = floatval( $request->input('requested_amount') );
			
		if( $current_balance < $amount_requested + ( Investment::WITHDRAWAL_FEE * $amount_requested ) )
		{
			return redirect()->back()->with('status', 'Sorry. Your balance on this investment is too low for the specified operation.');
		}
		
		$data = $request->all();
		
		$data['from_user_id'] = $user->id;
			
        $withdrawalRequest = WithdrawalRequest::create($data);
		
		  



//DEBIT
		$toBeActuallyDebited = floatval( $withdrawalRequest->requested_amount ) + ( Investment::WITHDRAWAL_FEE * $withdrawalRequest->requested_amount );
		
		{
			$investment = Investment::find($withdrawalRequest->from_investment_id);
			
			$investment->investment_total_interest_debit = $toBeActuallyDebited + floatval( $investment->investment_total_interest_debit );
		
		    $investment->save();
    }







        return redirect('/home')->with('status', 'Withdrawal Request created successfully. Admin will process your request.');
    }
}
