<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Investment extends Model
{
	public const WITHDRAWAL_FEE =  1.5/100;
	
	public const DEPOSIT_FEE = 1.5/100;
	
	public const ALL_INVESTMENT_PLANS = [
		"silver" => array("admin_fee" => "10", "duration" => 224, "interest_rate" => 0.8/100, "min_invest_amount" => 50, "max_invest_amount" => 50, "min_withdrawal" => 3, "max_withdrawal" => 1000000),
		"gold" => array("admin_fee" => "10", "duration" => 224, "interest_rate" => 1.6/100, "min_invest_amount" => 100, "max_invest_amount" => 100, "min_withdrawal" => 6, "max_withdrawal" => 1000000),
		"bronze" => array("admin_fee" => "20", "duration" => 224, "interest_rate" => 2.4/100, "min_invest_amount" => 200, "max_invest_amount" => 200, "min_withdrawal" => 12, "max_withdrawal" => 1000000),
		"diamond" => array("admin_fee" => "30", "duration" => 224, "interest_rate" => 3.2/100, "min_invest_amount" => 300, "max_invest_amount" => 300, "min_withdrawal" => 18, "max_withdrawal" => 1000000),
		"executive" => array("admin_fee" => "50", "duration" => 224, "interest_rate" => 4.0/100, "min_invest_amount" => 500, "max_invest_amount" => 500, "min_withdrawal" => 31, "max_withdrawal" => 1000000),
	];

	public static function getMinMaxInvestmentAmounts($plan)
	{
		$package = self::ALL_INVESTMENT_PLANS[$plan];

		$min = $package["min_invest_amount"];
		$max = $package["max_invest_amount"];

		return array($min, $max);
	}
	
	public static function getMinMaxWithdrawalAmounts($plan)
	{
		$package = self::ALL_INVESTMENT_PLANS[$plan];

		$min = $package["min_withdrawal"];
		$max = $package["max_withdrawal"];

		return array($min, $max);
	}
	
	public static function getDuration($plan)
	{
		$package = self::ALL_INVESTMENT_PLANS[$plan];

		$value = $package["duration"];

		return $value;
	}
	

	public static function getInterestRate($plan)
	{
		$package = self::ALL_INVESTMENT_PLANS[$plan];

		$interestRate = $package["interest_rate"];

		return $interestRate;
	}
	
	public function getDaysPast()
	{ 
		if($this->investment_status == "unconfirmed")
		{
			return 0;
		}
		
		$duration = self::getDuration($this->investment_type);

		$createdDate = new Carbon( $this->created_at );
		$endDate = Carbon::now();
		$difference = $createdDate->diff($endDate)->days;
		
		if($difference >= $duration)
		{
			return $duration;
		}
	}
	
    public function getBalance()
    {
		if($this->investment_status == "unconfirmed")
		{
			return 0.0;
		}
		
		if($this->investment_status == "inactive")
		{
			return floatval( $this->investment_total_interest_credit ) - floatval( $this->investment_total_interest_debit );
		}
		
		$createdDate = new Carbon( $this->created_at );
		$endDate = Carbon::now();
		$difference = $createdDate->diff($endDate)->days;
		$rate = $this->getInterestRate($this->investment_type);
		$investedAmount = $this->investment_amount;
		
		
		
		
		
		if($this->investment_type == "silver" && $difference >= 224)
		{
			if($this->investment_status != "expired")
			{ 
			   $user = User::find($this->investment_user_id);
			   
			   if(!empty($user))
			   {
			       RetonarMail::send($user->email, "Investment Expired", "One of your investments has expired.");
			   }
			
			   $this->investment_status = "expired";
			   $this->save();
			}
			
			return ( floatval($investedAmount) * 0.9 ) - floatval( $this->investment_total_interest_debit );
		}
		if($this->investment_type == "gold" && $difference >= 224)
		{
			if($this->investment_status != "expired")
			{
			   $user = User::find($this->investment_user_id);
			   
			   if(!empty($user))
			   {
			       RetonarMail::send($user->email, "Investment Expired", "One of your investments has expired.");
			   }
			
			   $this->investment_status = "expired";
			   $this->save();
			}
			
			$difference = 224;
		}
		if($this->investment_type == "bronze" && $difference >= 224)
		{
			if($this->investment_status != "expired")
			{
			   $user = User::find($this->investment_user_id);
			   
			   if(!empty($user))
			   {
			       RetonarMail::send($user->email, "Investment Expired", "One of your investments has expired.");
			   }
			
			   $this->investment_status = "expired";
			   $this->save();
			}
			
			$difference = 224;
		}
		if($this->investment_type == "diamond" && $difference >= 224)
		{
			if($this->investment_status != "expired")
			{
			   $user = User::find($this->investment_user_id);
			   
			   if(!empty($user))
			   {
			       RetonarMail::send($user->email, "Investment Expired", "One of your investments has expired.");
			   }
			
			   $this->investment_status = "expired";
			   $this->save();
			}
			
			$difference = 224;
		}
		if($this->investment_type == "executive" && $difference >= 224)
		{
			if($this->investment_status != "expired")
			{
			   $user = User::find($this->investment_user_id);
			   
			   if(!empty($user))
			   {
			       RetonarMail::send($user->email, "Investment Expired", "One of your investments has expired.");
			   }
			
			   $this->investment_status = "expired";
			   $this->save();
			}
			
			$difference = 224;
		}
		
		
		
		
		$interest = $difference * $rate * $investedAmount;
		
		$this->investment_total_interest_credit = $interest;
		
		$this->save();
		
		return floatval( $this->investment_total_interest_credit ) - floatval( $this->investment_total_interest_debit );
    }

	protected $primaryKey = 'investment_id';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'investment_amount', 'investment_user_id', 'investment_type', /*'investment_balance',*/ 'investment_status', 'investment_transaction_id',
		'investment_total_interest_credit', 'investment_total_interest_debit'
    ];
}
