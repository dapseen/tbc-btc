<?php

namespace App;

use Mail;

class RetonarMail
{
	public static function send($to, $subject, $msg)
	{
	   Mail::send('emails.general', ['to' => $to, 'subject' => $subject, 'msg' => $msg],
	      function ($message) use ($to, $subject, $msg)
	      {
	         $message->from(config('mail.from.address'), config('app.name'));
	         $message->to( $to );
	         $message->subject($subject); 
	      });
	}
	
	public static function sendWithLink($to, $subject, $msg, $vlink)
	{
	   Mail::send('emails.general', ['to' => $to, 'subject' => $subject, 'msg' => $msg, 'vlink' => $vlink],
	      function ($message) use ($to, $subject, $msg)
	      {
	         $message->from(config('mail.from.address'), config('app.name'));
	         $message->to( $to );
	         $message->subject($subject); 
	      });
	}
	public static function sendAgentForm($to, $subject,$msg, $info)
	{
		Mail::send('emails.agent_form',['to'=>$to,'subject'=>$subject,'msg'=>$msg,'info'=>$info],
			function ($message) use ($to,$subject,$msg)
			{
				$message->from(config('mail.from.address'), config('app.name'));
				$message->to( $to );
				$message->subject($subject);
			});
	}
}