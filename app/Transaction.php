<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
	protected $primaryKey = 'transaction_id';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transaction_amount', 'transaction_user_id', 'transaction_type', 'transaction_status', 'transaction_investment_id'
    ];
}
