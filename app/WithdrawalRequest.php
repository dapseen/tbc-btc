<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WithdrawalRequest extends Model
{
	protected $primaryKey = 'request_id';
	
    /**
     * The attributes that are mass assignable.
     *okay, testing nautilus
     * @var array
     */
    protected $fillable = [
        'iban_code', 'swift_code', 'requested_amount', 'from_user_id', 'from_investment_id', 'bank_account_name', 'bank_account_holder_name', 'bank_account_number', 'cypto_wallet_address', 'currency', 'withdrawal_method',
    ];
}
