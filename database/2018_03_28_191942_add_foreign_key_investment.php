<?php

/* moved because it was causing errors during migration */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyInvestment extends Migration
{
    //* Run the migrations.
    //*
    //* @return void
    //*
    public function up()
    {
         Schema::table('investments', function($table) {
             $table->integer('investment_user_id')->unsigned()->nullable()->change();
             
          $table->foreign('investment_user_id')->references('id')->on('users');
    });
    }

	//* Reverse the migrations.
    //*
    //* @return void
    //*
    public function down()
    {
        Schema::table('investments', function($table) {
        $table->dropForeign('investment_user');
    });
    }
}