<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawalRequestTableAddons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('withdrawal_requests', function (Blueprint $table) {
            $table->string('bank_account_name')->default("");
            $table->string('bank_account_holder_name')->default("");
            $table->string('bank_account_number')->default("");
            $table->string('cypto_wallet_address')->default("");
            $table->string('currency')->default("");
            $table->string('withdrawal_method');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('withdrawal_requests', function (Blueprint $table) {
            $table->dropColumn('bank_account_name');
            $table->dropColumn('bank_account_holder_name');
            $table->dropColumn('bank_account_number');
            $table->dropColumn('cypto_wallet_address');
            $table->dropColumn('currency');
            $table->dropColumn('withdrawal_method');
        });
    }
}
