<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
			
            $table->string('BTC_TO_USD')->default("0.0");
			$table->string('ETH_TO_USD')->default("0.0");
			$table->string('LTC_TO_USD')->default("0.0");
			$table->string('USD_TO_NGN')->default("0.0");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currency_rates');
    }
}
