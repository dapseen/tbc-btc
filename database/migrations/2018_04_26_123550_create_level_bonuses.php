<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelBonuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_bonuses', function (Blueprint $table) {
            $table->double('level_1_amount')->default(0);
            $table->double('level_2_amount')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_bonuses', function (Blueprint $table) {
            $table->dropColumn('level_1_amount');
			$table->dropColumn('level_2_amount');
        });
    }
}
