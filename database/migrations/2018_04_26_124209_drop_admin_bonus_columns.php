<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAdminBonusColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_bonuses', function (Blueprint $table) {
            $table->dropColumn(['rate_type', 'amount']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_bonuses', function (Blueprint $table) {
            $table->string('rate_type');
	    $table->string('amount');
        });
    }
}
