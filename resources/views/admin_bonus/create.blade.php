@extends('admin_layout.layouts')

@section('content')

<div class="container-fluid dashboard-container">

    <div class ="row">

        @include('admin_sidebar')

         <div class ="col-md-10 col-sm-10 col-xs-10 dashboard-body">

                <div class ="row">

                         <div class ="col-md-1">


                         </div>

                         <div class ="col-md-10">
                              
                           <div class ="row"> 
                              
                              <div class ="col-md-12">

                                   <!--<div class ="section-deading" style ="margin-bottom: 30px"></div>-->
                                  <div class ="" style ="height: auto; padding: 20px 30px; margin-bottom: 40px">

                                       @if (session('status'))
                                        <div class="alert alert-success">
                                         {{ session('status') }}
                                        </div>
                                       @endif


                          <form method="POST" action="/admin/bonus" id ="">
                                         @csrf

                                  		
                          <div class="dashboard-content-section bluetop-border" style="height: auto; padding: 20px 30px; margin-bottom: 40px;">
                                  <div class="row">
                                      <div class="form-group col-md-6">
                                      <label for="bank_account_name" class="col-md-12 col-form-label text-md-left"></label> 
                                      	<div class="col-md-12">
                                      		<h2>Level 1 Referral Bonus</h2>
											<br/>
                                       		<h2>Level 2 Referral Bonus</h2>
                                      	</div>
                                      </div>

                                       <div class="form-group col-md-6">

                                      <label for="amount" class="col-md-12 col-form-label text-md-left">Rate Amount in %</label> 
                                      <div class="col-md-12">
                                      <input id="level_1_amount" type="number" name="level_1_amount" value="{{App\AdminBonus::getLevel1BonusRate()}}" required="required" class="form-control">
									  </div>
									  <br/>
									  <div class="col-md-12">
                                      <input id="level_2_amount" type="number" name="level_2_amount" value="{{App\AdminBonus::getLevel2BonusRate()}}" required="required" class="form-control">
                                      </div>
									  
                                      </div>
                                  </div>
                                  <div class="form-group row mb-0">
                                    <div class="form-submit">
                                      <button type="submit" class="btn btn-primary">
                                        Create Bonus Rate
                                      </button>
                                                                  
                                    </div>
                                  </div>
                                                              
                    

                        
                    </form>

                                   
                                    </div>
                               </div>
                               

                            </div>

                         </div>


                         <div class ="col-md-1">


                         </div>

                </div>




         </div>









    </div>
    









</div>
@endsection
