@extends('admin_layout.layouts')

@section('content')

<div class="container-fluid dashboard-container">

    <div class ="row">

        @include('admin_sidebar')

         <div class ="col-md-10 col-sm-10 col-xs-10 dashboard-body">

                <div class ="row">

                         <div class ="col-md-1">


                         </div>

                         <div class ="col-md-10">
                              
                           <div class ="row"> 
                              
                              <div class ="col-md-12">

                                   <!--<div class ="section-deading" style ="margin-bottom: 30px"></div>-->
                                  <div class ="" style ="height: auto; padding: 20px 30px; margin-bottom: 40px">

                                       @if (session('status'))
                                        <div class="alert alert-success">
                                         {{ session('status') }}
                                        </div>
                                       @endif


                          <form method="POST" action="" id ="">
                                         @csrf

                                  		
                          <div class="dashboard-content-section bluetop-border" style="height: auto; padding: 20px 30px; margin-bottom: 40px;">
                                  <div class="row">
								  
								  
                                    <div class="form-group col-md-6">
									   
									   <label for="btc" class="col-md-12 col-form-label text-md-left">1 BTC TO USD</label> 
                                       <div class="col-md-12">
                                          <input id="btc" type="number" step="0.1" min="0" name="BTC_TO_USD" value="{{\App\CurrencyRates::getBTCUSDValue()}}" required="required" class="form-control">
                                       </div>
									   
									   <br/><label for="eth" class="col-md-12 col-form-label text-md-left">1 ETH TO USD</label> 
									   <div class="col-md-12">
                                          <input id="eth" type="number" step="0.1" min="0" name="ETH_TO_USD" value="{{\App\CurrencyRates::getETHUSDValue()}}" required="required" class="form-control">
									   </div>
									   
									   <br/><label for="ltc" class="col-md-12 col-form-label text-md-left">1 LTC TO USD</label> 
									   <div class="col-md-12">
										  <input id="ltc" type="number" step="0.1" min="0" name="LTC_TO_USD" value="{{\App\CurrencyRates::getLTCUSDValue()}}" required="required" class="form-control">
                                       </div>
									   
									   <!--<br/><label for="usd-ngn" class="col-md-12 col-form-label text-md-left">1 USD TO NGN</label> 
									   <div class="col-md-12">
                                          <input id="usd-ngn" type="number" step="0.1" min="0" name="USD_TO_NGN" value="{{\App\CurrencyRates::getUSDNGNValue()}}" required="required" class="form-control">
                                       </div>-->
									   
                                    </div>
									  
									  
                                  </div>
                                        <i style="padding-left: 15px;"></i>
										<button type="submit" class="btn btn-primary">
                                        Save Rates
                                      </button>                      
                    

                        
                    </form>

                                   
                                    </div>
                               </div>
                               

                            </div>

                         </div>


                         <div class ="col-md-1">


                         </div>

                </div>




         </div>









    </div>
    









</div>
@endsection
