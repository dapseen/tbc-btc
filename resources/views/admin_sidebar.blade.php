<div class ="col-md-2 dashboard-sidebar">


                <div class ="sidebar-content">


                   <div class ="user-welcome sidebar-div">

                    Welcome,<br><span style ="font-weight: 500">{{ Auth::user()->name }}</span>

                   </div>

                   <div class ="home sidebar-div">

                       <a href ="{{ url('/admin') }}"><i class="fa fa-home"></i> Home</a>

                   </div>

                   <div class ="all-users sidebar-div">

                       <a href ="{{ url('/admin/users') }}"><i class="fa fa-users"></i> View all Users</a>

                   </div>

                   <div class ="users sidebar-div sidebar-div-caret">

                        
                        <div><i class="fa fa-user-secret"></i> Admins</div>
                       <div><i class="fa fa-sort-down"></i></div>

                    </div>

                    <ul class ="sub-menu user-sub-menu">

                            <a href ="{{ url('/admin/admins') }}"><li>All Admins</li></a>

                            <a href ="{{ url('/admin/create') }}"><li>Create Sub-Admin</li></a>

                            
                    </ul>


				   
				   
                   <div class ="all-users sidebar-div">
				   
						   <a href = "{{url('admin/investments')}}"> <i class="fa fa-signal"></i> All Investments </a>
						   
                   </div>
				   
				   
				   
					
                   

                    <div class ="transactions sidebar-div sidebar-div-caret">

                        
                        <div><i class="fa fa-bar-chart"></i> Transactions</div>
                       <div><i class="fa fa-sort-down"></i></div>

                    </div>
                    <ul class ="sub-menu transactions-sub-menu">
                        <a href = "{{url('admin/transactions')}}"><li>All Transactions</li></a>

                        <a href = "{{url('admin/transactions#tranwithdrawal')}}" class ="tranwithdrawal"><li>Withdrawal</li></a>

                        <a href = "{{url('admin/transactions#trandeposit')}}" class="trandeposit"><li>Deposit</li></a>
                    </ul>

                    

                    <div class ="home sidebar-div">

                       <a href = "{{url('admin/withdrawal/requests')}}"><i class="fa fa-tasks"></i> Withdrawal Requests</a>

                   </div>
		 <div class ="manage-rate sidebar-div sidebar-div-caret">

                        
                        <div><i class="fa fa-inbox"></i> Manage Rate</div>
                       <div><i class="fa fa-sort-down"></i></div>

                    </div>
                    <ul class ="sub-menu rate-sub-menu">
                        <a href = "{{url('admin/bonus')}}"><li>Manage All Rate</li></a>

                       
                    </ul>


                </div>


         </div>
