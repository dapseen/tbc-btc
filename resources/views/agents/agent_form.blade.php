<div id="agent-form" style="display: none;">
    <form method="POST" action="/home/pay-to-agent" id="register-form">
        @csrf
        <div class="row">
            <div class="form-group col-md-4">
                <label for="name" class="col-md-12 col-form-label text-md-left">{{ __('Amount') }} in &dollar;</label>

                <div class="col-md-12">
                    <input id="amount" type="number"  class="form-control" name="amount" value="{{ old('amount') }}" required autofocus> @if ($errors->has('amount'))
                    <span class="invalid-feedback">
											        <strong>{{ str_replace("amount", "amount in dollars", $errors->first('amount') ) }}</strong>
                                                </span> @endif
                </div>

            </div>
            <div class="form-group col-md-4">
                <label for="name" class="col-md-12 col-form-label text-md-left">{{ __('Country') }} </label>

                <div class="col-md-12">
                    <input id="country" type="text" class="form-control" name="country" placeholder="Singapore" required autofocus> 
					<input type="hidden" name="name" value="{{ Auth::user()->name }}"/>
					<input type="hidden" name="email" value="{{ Auth::user()->email }}" />
					<input type="hidden" name="plan" value="{{$plan}}"/>
                </div>

            </div>
            <div class="form-group col-md-4">
                <label for="name" class="col-md-12 col-form-label text-md-left">{{ __('Mobile') }} </label>
                <div class="col-md-12">
                    <input id="mobile" type="number" class="form-control" name="mobile" placeholder="+21" required autofocus> 
                    
                </div>
            </div>
        </div>

        <div style="padding-left: 15px;">
            <button type="submit" class="btn btn-primary">
                {{ __('Pay') }}
            </button>
        </div>

    </form>
</div>


	<script>	
	    function showAgentForm(){
			openModal("agent-form");
		}
	</script>
    @if ($errors->has('amount') && !empty(old('eth-amount')) )
        <script>showAgentForm();</script>            
	@endif
	

