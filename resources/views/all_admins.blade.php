@extends('admin_layout.layouts')

@section('content')

<div class="container-fluid dashboard-container">

    @if (session('status'))
        <div class="alert alert-success" align="center">
            {{ session('status') }}
        </div>
    @endif
	
    <div class ="row">

        @include('admin_sidebar')

         <div class ="col-xs-10 col-md-10 dashboard-body">

                <div class ="row">

                         <div class ="col-md-1">


                         </div>

                         <div class ="col-md-10">
                              
                           <div class ="row"> 
                              
                              <div class ="col-md-12">

                                   <!--<div class ="section-deading" style ="margin-bottom: 30px">{{ count($admins) }} Admins</div>-->
                                  <div class ="dashboard-content-section" style ="height: auto; padding: 0px">

                                  	<div class ="table-responsive">

                                    <table class="table">
                                   
                                     <thead>
                                       <tr>
                                          <th>User name</th>
                                          <th>Email Address</th>
                                          <th>User Manager</th>
                                          <th>Withdrawal Manager</th>
                                       </tr>
                                     </thead>
                                     <tbody>
                                        @foreach ($admins as $admin)
                                         <tr>
                                           <td>{{ $admin->name }}</td>
                                           <td>{{ $admin->email }}</td>
                                           <td>{{ $admin->user_manager  ? "YES":"NO" }}</td>
                                           <td>{{ $admin->withdrawal_manager ? "YES":"NO" }}</td>
                                           <td><a href = '#' onclick="if(confirm('Are you sure you want to delete {{ $admin->name }}?')) {window.location = 'delete/{{ $admin->id }}';} else {};">Delete</a></td>
                                           <td><a href = '#' onclick="if(confirm('Are you sure you want to block {{ $admin->name }}?')) {window.location = 'block/{{ $admin->id }}';} else {};">Block</a></td>
                                          </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                    </div>
                                    </div>
                               </div>
                               

                            </div>

                         </div>


                         <div class ="col-md-1">


                         </div>

                </div>




         </div>









    </div>
    








</div>
@endsection
