@extends('layouts.app')

@section('content')

<div class="container-fluid dashboard-container">

    @if (session('status'))
        <div class="alert alert-success" align="center">
            {{ session('status') }}
        </div>
    @endif
	
    <div class ="row">

        @include('admin_sidebar')

         <div class ="col-md-10 dashboard-body">

                <div class ="row">

                         <div class ="col-md-1">


                         </div>

                         <div class ="col-md-10">
                              
                           <div class ="row"> 
                              
                              <div class ="col-md-12">

                                  <div class ="dashboard-content-section" style ="height: auto; padding: 15px; margin: 0px 0px 30px 0px; display: flex; justify-content: space-between;">

                                    <div class ="heading" style="padding-top: 6px">All Investments</div>
                                    

                                    <div><input type="text" class="form-control" id="search-investments" onkeyup="searchInvestments()" placeholder="Owner"></div>


                                  </div>

                                  <div class ="dashboard-content-section" style ="height: auto; padding: 0px">

                                  <div class ="table-responsive">

                                    <table class="table" id ="all-investments-table">
                                   
                                     <thead>
                                       <tr>
                                          <th>Owned By</th>
                                          <th>Date</th>
                                          <th>Investment Plan</th>
                                          <th>Amount Invested</th>
                                          <th>Withdrawable Balance</th>
                                          <th>Investment Status</th>
										  
                                          <th></th>
                                       </tr>
                                     </thead>
                                     <tbody>
                                        @foreach ($investments as $investment)
										@php $btn = $investment->investment_status != "active" ? "activate" : "deactivate"; @endphp
                                         <tr>
                                           <td>{{ $usernames[$investment->investment_user_id] }}</td>
                                           <td>{{ Carbon\Carbon::parse($investment->created_at)->format('m/d/Y') }}</td>
                                           <td>{{ $investment->investment_type }}</td>
                                           <td>{{ $investment->investment_amount }}</td>
                                           <td>{{ $investment->getBalance() }}</td>
                                            <td>{{ $investment->investment_status }}</td>
                                           
										   <td><a class="" href='#' onclick="if(confirm('Are you sure you want to {{$btn}} this investment?')) {window.location = '{{url('admin/investments/'.$btn.'/'.$investment->investment_id)}}';} else {} return false;"><i class="fa fa-user-times text-danger"></i></a></td>
                                           </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </div>
                                    </div>
                               </div>
                               

                            </div>

                            
							
                        </div>
						 
						 <div class ="col-md-1">


                         </div>

                </div>




         </div>









    </div>
    
						 
                         
</div>
@endsection
