@extends('layouts.app') @section('content')

<div class="container-fluid dashboard-container">

    @if (session('status'))
    <div class="alert alert-success" align="center">
        {{ session('status') }}
    </div>
    @endif

    <div class="row">

        @include('admin_sidebar')

        <div class="col-md-10 dashboard-body">

            <div class="row">

                <div class="col-md-1">

                </div>

                <div class="col-md-10">

                    <div class="row">

                        <div class="col-md-12">

                            <div class ="dashboard-content-section" style ="display: flex; justify-content: space-between; height: auto; padding: 15px; margin: 0px 0px 30px 0px">

                                    <div class ="heading" style="padding-top: 6px">Transactions</div>
                                    

                                    <div>
                                        <input type="text" class="form-control" id="search-transactions" onkeyup="searchTransactions()" placeholder="Search Transactions">
                                    </div>


                            </div>

                            <div class="transactions-body">

                             <div class="table-responsive">

                                <table class="transaction-table table" id ="transaction-table">

                                    <thead>
                                        <tr>
                                            <th>Owned By</th>
                                            <th>Date</th>
                                            <th>Category</th>
                                            <th>Amount</th>
                                            <th>Transaction Status</th>
                                            
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach ($transactions as $transaction)
                                        <tr class="filter-{{$transaction->transaction_type}}">

                                            <td>{{ $usernames[$transaction->transaction_user_id] }}</td>
                                            <td>{{ Carbon\Carbon::parse($transaction->created_at)->format('m/d/Y') }}</td>
                                            <td><span class="category-{{$transaction->transaction_type}}">{{$transaction->transaction_type}}</span></td>
                                            <td>&dollar; {{$transaction->transaction_amount}}</td>
                                            <td>{{ $transaction->transaction_status }}</td>
                                            
                                        </tr>
                                        @endforeach

                                    </tbody>

                                </table>

                              </div>

                            </div>

                        </div>

                    </div>

                    <div class="col-md-1">

                    </div>

                </div>

            </div>

        </div>

    </div>
    @endsection