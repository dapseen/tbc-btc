@extends('layouts.app')

@section('content')

<div class="container-fluid dashboard-container">

    @if (session('status'))
        <div class="alert alert-success" align="center">
            {{ session('status') }}
        </div>
    @endif
	
    <div class ="row">

        @include('admin_sidebar')

         <div class ="col-xs-10 col-md-10 dashboard-body">

                <div class ="row">


                         <div class ="col-md-12">
                              
                           <div class ="row"> 
                              
                              <div class ="col-md-12">

                                  <div class ="dashboard-content-section" style ="height: auto; padding: 0px">
                                <div class="table-responsive">
                                    <table class="table">
                                   
                                     <thead>
                                       <tr>
                                          <th>Owned By</th>
										  <th>Bank Account Name</th>
                                          <th>Bank Account Holder Name</th>
                                          <th>Bank Account Number</th>
										  <th>Country</th>
                                          <th>SWIFT/Routing Number</th>
                                          <th>Wallet Address</th>
                                          <th>Currency</th>
                                          <th>Requested Amount</th>
                                          
                                       </tr>
                                     </thead>
                                     <tbody>
                                        @foreach ($requests as $withdrawalRequest)
										<tr>
                                           <td>{{ $usernames[$withdrawalRequest->from_user_id] }}</td>
										   <td>{{ $withdrawalRequest->bank_account_name }}</td>
                                           <td>{{ $withdrawalRequest->bank_account_holder_name }}</td>
                                           <td>{{ $withdrawalRequest->bank_account_number }}</td>
										   <td>{{ $withdrawalRequest->iban_code }}</td>
                                           <td>{{ $withdrawalRequest->swift_code }}</td>
                                           <td>{{ $withdrawalRequest->cypto_wallet_address }}</td>
                                           <td>{{ $withdrawalRequest->currency }}</td>
                                           <td>{{ $withdrawalRequest->requested_amount }} USD</td>
										   <td><a href = '#' onclick="if(confirm('Are you sure you want to accept this request?')) {window.location = '/admin/withdrawal/requests/accept/{{$withdrawalRequest->request_id}}';} else {};"><i class="fa fa-check text-primary"></i></a></td>
										   <td><a href = '#' onclick="if(confirm('Are you sure you want to reject this request?')) {window.location = '/admin/withdrawal/requests/reject/{{$withdrawalRequest->request_id}}';} else {};"><i class="fa fa-trash text-danger"></i></a></td>
										 </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                </div>
                                    </div>
                               </div>
                               

                            </div>

                            
							
                        </div>
						 
						

                </div>




         </div>









    </div>
    
						 
                         
</div>
@endsection
