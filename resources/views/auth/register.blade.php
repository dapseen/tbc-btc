@extends('layouts.app')

@section('content')
@include('modals')
<div class="container-fluid" id ="register-js">
    <div class ="row form-header">

	<!-- bad
	
	{{ $refid = isset($_GET['refid']) && !empty($_GET['refid']) ? $_GET['refid'] : 'admin' ,
	   $disabledvar = !empty($refid)?'style=""' : ''
	}}
	
	bad
	-->

    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card form-card">
               
                <p class ="form-heading">Start Exchanging TBC to BTC</p>
               
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}?referral_id={{ $refid }}" id ="register-form">
                        @csrf

                        <div class ="row">	
                            <div class="form-group col-md-12">
                                <label for="referral_id" class="col-md-12 col-form-label text-md-left">{{ __('Referral ID (Referred By)') }}</label>
    
                                <div class="col-md-12">
                                    <i id="referral_id" class="form-control{{ $errors->has('referral_id') ? ' is-invalid' : '' }}" name="referral_id">{{ $refid }}</i>
    
                                    @if ($errors->has('referral_id'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('referral_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                    </div>


                    <div class ="row">
                          <div class="form-group col-md-6">
                             <label for="name" class="col-md-12 col-form-label text-md-left">{{ __('Username') }}</label>

                              <div class="col-md-12">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                             </div>
                          </div>

                        <div class="form-group col-md-6">
                            <label for="email" class="col-md-12 col-form-label text-md-left">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    </div>
						
						
		
						
						
						
					<div class ="row">	
						<div class="form-group col-md-6">
                            <label for="first_name" class="col-md-12 col-form-label text-md-left">{{ __('First Name') }}</label>

                            <div class="col-md-12">
                                <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus>

                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
							
						
						<div class="form-group col-md-6">
                            <label for="last_name" class="col-md-12 col-form-label text-md-left">{{ __('Last Name') }}</label>

                            <div class="col-md-12">
                                <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus>

                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
					</div>
						
						
						
						
						
						
					<div class="row">
						<div class="form-group col-md-6">
                            <label for="bank_account_name" class="col-md-12 col-form-label text-md-left">{{ __(' ') }}</label>

                            <div class="col-md-12">
<input id="bank_account_name" class="form-control{{ $errors->has('bank_account_name') ? ' is-invalid' : '' }}" name="bank_account_name" value="-" type="hidden" />
                              
                                @php /* @if ($errors->has('bank_account_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('bank_account_name') }}</strong>
                                    </span>
                                @endif */ @endphp
                            </div>
                        </div>
						
				
				        <div class="form-group col-md-6">
                            <label for="account_number" class="col-md-12 col-form-label text-md-left">{{ __(' ') }}</label>

                            <div class="col-md-12">
                                <input type="hidden" id="account_number" class="form-control{{ $errors->has('account_number') ? ' is-invalid' : '' }}" name="account_number" value="00" autofocus>

                                @php /* @if ($errors->has('account_number'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('account_number') }}</strong>
                                    </span>
                                @endif */ @endphp
                            </div>
                        </div>
					</div>
						
					
                    <div class ="row">
                        <div class="form-group col-md-6">
                            <label for="password" class="col-md-12 col-form-label text-md-left">{{ __('Password') }}</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="password-confirm" class="col-md-12 col-form-label text-md-left">{{ __('Confirm Password') }}</label>

                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                    </div>

                     <div class ="row">
                        <div class="form-group col-md-6">
                            

                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label><input type="checkbox" required="">   I have accepted the <span style ="color: #42beef" class ="open_terms_register">Terms of Use</span></label>
                                </div>
                            </div>
                        </div>
                    </div>

                   

                        <div class="form-group row mb-0">
                            <div class="form-submit">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                                
                            </div>
                            
                        </div>

                        <p class ="form-meta">Already have an account ? <a class="" href="{{ route('login') }}">{{ __('Login') }}</a></p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')
@endsection
