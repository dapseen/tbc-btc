<div id="btc-calc" style="display: none;">
    <form method="POST" action="" id="register-form">
        @csrf

        <div class="row">
            <div class="form-group col-md-6">
                <label for="name" class="col-md-12 col-form-label text-md-left">{{ __('Amount') }} in &dollar;</label>

                <div class="col-md-12">
                    <input onkeyup="amount1Updated_BTC();" id="name" type="number" step="0.1" min="0" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" value="{{ old('amount') }}" required autofocus> @if ($errors->has('amount'))
                    <span class="invalid-feedback">
											        <strong>{{ str_replace("amount", "amount in dollars", $errors->first('amount') ) }}</strong>
                                                </span> @endif

                    <label for="name" class="col-md-12 col-form-label text-md-left">{{ __('Amount') }} in BTC</label>
                    <input onkeyup="amount2Updated_BTC();" id="name" class="form-control" type="number" step="0.00000001" min="0" name="btc-amount" value="{{ old('btc-amount') }}" required autofocus>
					
					<input type="hidden" name="sendto" value="true"/>
					<input type="hidden" name="currency2" value="BTC"/>
					<input type="hidden" name="plan" value="{{$plan}}"/>
                </div>

            </div>
        </div>

        <div style="padding-left: 15px;">
            <button type="submit" class="btn btn-primary">
                {{ __('Deposit') }}
            </button>
        </div>

    </form>
</div>


	<script>
		var BTC_RATE = {{\App\CurrencyRates::getBTCUSDValue()}};
			
	    function showBTCCalculator(){
			openModal("btc-calc");
		}
		function amount1Updated_BTC(){
			
			var amount1 = document.getElementsByName("amount")[0];
			var amount2 = document.getElementsByName("btc-amount")[0];
			
			amount2.value = (amount1.value / BTC_RATE).toFixed(8);
		}
		function amount2Updated_BTC(){
			var amount1 = document.getElementsByName("amount")[0];
			var amount2 = document.getElementsByName("btc-amount")[0];
			
			amount1.value = (amount2.value * BTC_RATE).toFixed(2);
		}
	</script>
	
	@if ($errors->has('amount') && !empty(old('btc-amount')) ) 
        <script>showBTCCalculator();</script>            
	@endif