<div id="card-calc" style="display: none;">
    <form method="POST" action="-" id="register-form" onsubmit="charge(); return false;">
        @csrf

        <div class="row">
            <div class="form-group col-md-6">
                <label for="name" class="col-md-12 col-form-label text-md-left">{{ __('Amount') }} in &dollar;</label>

                <div class="col-md-12">
                    <input id="card-amount" type="number" step="0.1" min="{{\App\Investment::getMinMaxInvestmentAmounts($plan)[0]}}" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" value="{{ old('amount') }}" required autofocus> @if ($errors->has('amount'))
                    <span class="invalid-feedback">
											        <strong>{{ str_replace("amount", "amount in dollars", $errors->first('amount') ) }}</strong>
                                                </span> @endif
												
					<input type="hidden" name="sendto" value="true"/>
					<input type="hidden" name="currency2" value="LTC"/>
					<input type="hidden" name="plan" value="{{$plan}}"/>
                </div>

            </div>
        </div>

        <div style="padding-left: 15px;">
            <button type="submit" class="btn btn-primary">
                {{ __('Deposit') }}
            </button>
        </div>

    </form>
</div>

<form method="POST" action="" id="doit-form" style='display: none;'>
	@csrf
    <input type="hidden" name='amount' id="doit-amount" value="" />
	<input type="hidden" name="plan" value="{{ $plan }}" />
</form>

	<script src="https://api.ravepay.co/flwv3-pug/getpaidx/api/flwpbf-inline.js"></script>
	<script>
	
	
	/*setInterval(function()
	{
		var iframe = document.getElementById("flwpugpaidid");
		//var element = iframe.contentWindow.document.getElementsByClassName("fw-copyright")[0];
		
		iframe.style.height = "40px"; 
		iframe.style.overflow = "hidden"; 
	}
	, 700);
    */
	
	
	
	
	
	
	
	
	    function showCardCalculator(){
			openModal("card-calc");
		}
		
		
	    function charge(){
			
			var amt = document.getElementById('card-amount').value;
			
			closeModal();
			
			const API_publicKey = "FLWPUBK-aa17c6d772bb3ed33b033dec71f70440-X";

    
        var x = getpaidSetup({
            PBFPubKey: API_publicKey,
            customer_email: "maarkelv@gmail.com",
            amount: amt,
            customer_phone: "234813936008",
            currency: "USD",
            payment_method: "card",
            txref: "retonar-txRef-" + Math.random(),
            meta: [{
                metaname: "investment plan by {{\Auth::user()->name}} ({{\Auth::user()->email}})",
                metavalue: "{{ $plan }} plan"
            }],
            onclose: function() {},
            callback: function(response) {
                var txref = response.tx.txRef; 
				// collect flwRef returned and pass to a server page to complete status check.
                console.log("This is the response returned after a charge", response);
                if (
                    response.tx.chargeResponseCode == "00" ||
                    response.tx.chargeResponseCode == "0"
                ) 
				{
                    document.getElementById('doit-amount').value = amt;
					document.getElementById('doit-form').submit();
                } else {
                    alert('Deposit Failed.');
                }

                x.close();
				// use this to close the modal immediately after payment.
            }
        });
    }
	</script>
	
	
	
	@if ($errors->has('amount') && !empty(old('ltc-amount')) )
        <script>showCardCalculator();</script>            
	@endif
