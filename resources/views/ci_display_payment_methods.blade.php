@extends('layouts.app') @section('content')

<div class="container-fluid dashboard-container">

    <div class="row">

        @include('sidebar')
		
		@include('modal_dialog')

        <div class="col-md-10 col-sm-10 col-xs-10 dashboard-body">

            <div class="row">

                <div class="col-md-1">

                </div>

                <div class="col-md-10">

                    <!--<h1>{{strtoupper($plan)}} Investment Plan</h1>-->

                    <div class="row" style="margin: 15px">

                        <div class="col-md-12">

                            
							
							
							
							
							
                            
                            <div class ="section-heading" style ="margin-bottom: 10px">{{strtoupper($plan)}} Investment Plan</div>
                             <div class ="flex-box">
                               
                            <div class="dashboard-content-section col-md-6 payment-method" style="height: auto; padding: 15px 0px; border-top: 5px solid #C99d66">

                              <a href="#" onclick="showETHCalculator()">

                                <div class ="payment-method-header etherum-pay"><img src="{{asset('images/etherumpay.png')}}" />Ethereum</div>

                                <div class ="payment-method-body">


                                      Making payment via Ethereum is easy, you just have to transfer the amount of ETH to our walet ID
                                </div>

                               </a>

                            </div>

                        </div>

                        <div class="flex-box">
                            <div class="dashboard-content-section col-md-6 payment-method" style="height: auto; padding: 15px 0px; border-top: 5px solid #989898">

                              <a href="#" onclick="showLTCCalculator()">

                                <div class ="payment-method-header litecoin-pay"><img src="{{asset('images/litecoinpay.png')}}" /> Pay With Litecoin</div>

                                <div class ="payment-method-body">


                                       Making payment via Litecoin is very easy, as you just have to transfer the amount of LTC to our Wallet ID.

                                </div>
                               </a>

                            </div>
                            <!--<div class="dashboard-content-section col-md-6 payment-method" style="height: auto; padding: 15px 0px; border-top: 5px solid #09a5db">

                              <a href="#" onclick="payByPaystack()">

                                <div class ="payment-method-header paystack-pay"><img src="{{asset('images/paystackpay.png')}}" /> Pay With Paystack</div>

                                <div class ="payment-method-body">


                                       Vivamus elementum semper nisi. Ut id nisl quis enim dignissim sagittis. Praesent nonummy mi in odio. Quisque id odio. 

                                </div>
                              </a>

                            </div>
                        </div> -->

								<div>
								  <!--  <h2>Choose your payment method</h2>
									
									<a href="#" onclick="showBTCCalculator()"> <img src="{{asset('images/bitcoin-pay.png')}}" /> </a>
									<a href="#" onclick="showETHCalculator()"> <img src="{{asset('images/ethereum-pay.png')}}" /> </a>
									<br/><br/><br/><br/>
									<a href="#" onclick="showLTCCalculator()"> <img src="{{asset('images/litecoin-pay.jpg')}}" /> </a>
									<a href="#" onclick="payByPaystack()"> <img src="{{asset('images/card-pay.png')}}" /> </a>-->
								</div>
								
								
								
								<div class="dashboard-content-section col-md-6 payment-method" style="height: auto; padding: 15px 0px; border-top: 5px solid #FF9900">

                              <a href="#" onclick="showBTCCalculator()">

                                    <div class ="payment-method-header bitcoin-pay"><img src="{{asset('images/bitcoinpay.png')}}" /> Pay With Bitcoin</div>

                                    <div class ="payment-method-body">


                                       Making payment via Bitcoin is very easy, as you just have to transfer the amount of BTC to our Wallet ID. 

                                    </div>
                                </a>

                            </div>
								
							</div>
                        </div>

                            <div class ="flex-box">
                               
								
								@include('btc_calculator')
								@include('eth_calculator')
								@include('ltc_calculator')

                                
									
                            
                        

                    </div>

                </div>

                <div class="col-md-1">

                </div>

            </div>

        </div>

    </div>

</div>
@endsection

