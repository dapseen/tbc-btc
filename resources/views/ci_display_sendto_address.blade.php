@extends('layouts.app') @section('content')

<div class="container-fluid dashboard-container">

    <div class="row">

        @include('sidebar')
		
		@include('modal_dialog')

        <div class="col-md-10 col-sm-10 col-xs-10 dashboard-body">

            <div class="row">

                <div class="col-md-1">

                </div>

                <div class="col-md-10">

                    <h1 class ="expecting">Expecting {{$crypto_amount}} {{$crypto_sign}} at this address:</h1>

                    <div class="row">

                        <div class="col-md-12">

                            <!--<div class ="section-deading" style ="margin-bottom: 30px"></div>-->
                            <div class="dashboard-content-section" style="height: auto; padding: 20px 30px; margin-bottom: 40px">

								<div style="text-align: center;">
								    <h2 style="font-size: 18px"> {{$wallet_details[0]}}</h2>
									
									<img src="{{$wallet_details[1]}}" />							
								</div>

                                <div style="text-align: center;" class ="text-danger">
                                <p>
                                Do you want to send {{$crypto_amount}} {{$crypto_sign}} (&dollar;{{$amount}} worth) ?
                                </p>
                                <p>
                                    &dollar;{{$admin_fee}} Admin Fee has already been added to it
                                </p>

                                </div>
								
								
								<form method="POST" action="" id ="register-form">
								@csrf
                                    <input type="hidden" name="amount" value="{{ $amount-$admin_fee }}" />
									<input type="hidden" name="plan" value="{{ $plan }}" />
									
                                    <div style="">
                                        <button type="submit" class="btn btn-primary">
                                            Yes
                                        </button>
                                    </div>
                                </form>
								
                            </div>
                        </div>

                    </div>

                </div>

                <div class="col-md-1">

                </div>

            </div>

        </div>

    </div>

</div>
@endsection