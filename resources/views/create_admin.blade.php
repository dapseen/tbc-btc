@extends('admin_layout.layouts')

@section('content')

<div class="container-fluid dashboard-container">

    <div class ="row">

        @include('admin_sidebar')

         <div class ="col-md-10 col-sm-10 col-xs-10 dashboard-body">

                <div class ="row">

                         <div class ="col-md-1">


                         </div>

                         <div class ="col-md-10">
                              
                           <div class ="row"> 
                              
                              <div class ="col-md-12">

                                   <!--<div class ="section-deading" style ="margin-bottom: 30px"></div>-->
                                  <div class ="dashboard-content-section" style ="height: auto; padding: 20px 30px; margin-bottom: 40px">

                                       @if (session('status'))
                                        <div class="alert alert-success">
                                         {{ session('status') }}
                                        </div>
                                       @endif


                                        <form method="POST" action="/admin/create" id ="create-user-form">
                                         @csrf

                        


                    <div class ="row">
                          <div class="form-group col-md-6">
                             <label for="name" class="col-md-12 col-form-label text-md-left">{{ __('Username') }}</label>

                              <div class="col-md-12">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                             </div>
                          </div>

                        <div class="form-group col-md-6">
                            <label for="email" class="col-md-12 col-form-label text-md-left">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    </div>  
            
            
          
            
          
                    <div class ="row">
                        <div class="form-group col-md-6">
                            <label for="password" class="col-md-12 col-form-label text-md-left">{{ __('Password') }}</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="password-confirm" class="col-md-12 col-form-label text-md-left">{{ __('Confirm Password') }}</label>

                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                    </div>

                    <div class ="row">
                        <div class="form-group col-md-12">
                            <label for="roles" class="col-md-12 col-form-label text-md-left">{{ __('Roles') }}</label>

                            <div class="col-md-12">
                                
                                <div class="checkbox">
                                  <label><input type="checkbox" name ="user_manager" value="true"> User Manager</label>
                                </div>
                                <div class="checkbox">
                                  <label><input type="checkbox" name="withdrawal_manager" value="true"> Withdrawal Manager</label>
                                </div>
                                

                                @if ($errors->has('roles'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('roles') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                    </div>

                   

                        <div class="form-group row mb-0">
                            <div class="form-submit">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Create User') }}
                                </button>
                                
                            </div>
                            
                        </div>

                        
                    </form>

                                   
                                    </div>
                               </div>
                               

                            </div>

                         </div>


                         <div class ="col-md-1">


                         </div>

                </div>




         </div>









    </div>
    









</div>
@endsection
