@extends('layouts.app')

@section('content')

<div class="container-fluid dashboard-container">

    <div class ="row">

        @include('sidebar')

         <div class ="col-md-10 col-sm-10 col-xs-10 dashboard-body">

                <div class ="row">
				
                         <div class ="col-md-1">


                         </div>

                         <div class ="col-md-10">
                              
				<h1>{{strtoupper($plan)}} Investment Plan</h1>
				
                           <div class ="row"> 
                              
                              <div class ="col-md-12">

                                   <!--<div class ="section-deading" style ="margin-bottom: 30px"></div>-->
                                  <div class ="dashboard-content-section" style ="height: auto; padding: 20px 30px; margin-bottom: 40px">

                                       @if (session('status'))
                                        <div class="alert alert-success">
                                         {{ session('status') }}
                                        </div>
                                       @endif


					
					
					
					
					
                                        <form method="POST" action="?plan={{$plan}}" id ="register-form">
                                         @csrf

                        


                    <div class ="row">
                          <div class="form-group col-md-6">
                             <label for="name" class="col-md-12 col-form-label text-md-left">{{ __('Amount') }} &dollar;</label>

                              <div class="col-md-12">
                                <input id="name" type="number" step="0.1" min="0" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" value="{{ old('amount') }}" required autofocus>

                                @if ($errors->has('amount'))
                                    <span class="invalid-feedback">
                                        <strong>{{ str_replace("amount", "amount in dollars", $errors->first('amount') ) }}</strong>
                                    </span>
                                @endif
                             </div>
                          </div>
					</div>
						 
					<div  style="padding-left: 15px;">
						 <button type="submit" class="btn btn-primary">
                                    {{ __('Deposit') }}
                        </button>
					</div>
                        
                    </form>

                                   
                                    </div>
                               </div>
                               

                            </div>

                         </div>


                         <div class ="col-md-1">


                         </div>

                </div>




         </div>









    </div>
    









</div>
@endsection
