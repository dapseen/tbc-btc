<div class="text-center toggle_admin_plans">

<div class ="text-center">...</div>


<span>Make New Investment</span>



</div>

<div class ="row admin_plans" id="investment_plans">	
   
@foreach(\App\Investment::ALL_INVESTMENT_PLANS as $packageName => $package)
<div class ="col-md-4 col-xs-6">	
    <div class ="plan-one admin-plan-option">

 
    <div class ="text-center text-heading">{{$packageName}}</div> 

    <div class ="text-content" style="text-align: center;">

           With this plan you get {{$package["interest_rate"] * 100}} percent every day.

    </div>
    <div class ="plan-action text-center">

       <a href='{{ url("/home/invest/$packageName") }}' class ="button">Choose Plan</a>
         

    </div>

    </div>
</div>
@endforeach
	
	
   
</div>