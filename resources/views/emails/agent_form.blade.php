<!DOCTYPE html>
<html>
<head>
    <title>{{config('app.name')}}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
</head>
 
<body>
<h2>New Agent Form from {{config('app.name')}}. </h2>
<br/>
<div class="container">
    <p>
         {{$msg}}
    </p>

    <div class="table-responsive">
        <table class="table">
               <thead>
                    <tr>
                        <th scope="col">label </th>
                        <th scope="col">Information </th>
                    </tr>
               </thead>

               <tbody>
                    <tr>
                        <th> Username</th>
                        <td>{{$info['name']}}  </td>
                    </tr>    
                    <tr>
                        <th> Country</th>
                        <td>{{$info['country']}}  </td>
                    </tr> 
                    <tr>
                        <th> Amount</th>
                        <td> {{$info['amount']}} USD </td>
                    </tr> 

                     <tr>
                        <th> Email</th>
                        <td>  {{$info['email']}}</td>
                    </tr> 
                    <tr>
                        <th> Mobile</th>
                        <td>  {{$info['mobile']}}</td>
                    </tr> 
                    <tr>
                        <th> Plan</th>
                        <td> {{$info['plan']}} </td>
                    </tr> 
               </tbody>
        </table>
    </div>
   
</div>

</body>
 
</html>