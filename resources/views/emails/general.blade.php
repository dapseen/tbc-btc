<!DOCTYPE html>
<html>
<head>
    <title>{{config('app.name')}}</title>
</head>
 
<body>
<h2>Hello from {{config('app.name')}}. </h2>
<br/>
{{$msg}}

@php if(isset($vlink))
    { @endphp <br/><a href='{{$vlink}}'>{{$vlink}}</a> @php }
@endphp

</body>
 
</html>