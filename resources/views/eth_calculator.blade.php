<div id="eth-calc" style="display: none;">
    <form method="POST" action="" id="register-form">
        @csrf

        <div class="row">
            <div class="form-group col-md-6">
                <label for="name" class="col-md-12 col-form-label text-md-left">{{ __('Amount') }} in &dollar;</label>

                <div class="col-md-12">
                    <input onkeyup="amount1Updated_ETH();" id="name" type="number" step="0.1" min="0" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" value="{{ old('amount') }}" required autofocus> @if ($errors->has('amount'))
                    <span class="invalid-feedback">
											        <strong>{{ str_replace("amount", "amount in dollars", $errors->first('amount') ) }}</strong>
                                                </span> @endif

                    <label for="name" class="col-md-12 col-form-label text-md-left">{{ __('Amount') }} in ETH</label>
                    <input onkeyup="amount2Updated_ETH();" id="name" class="form-control" type="number" step="0.000000000000000001" min="0" name="eth-amount" value="{{ old('eth-amount') }}" required autofocus>
					
					<input type="hidden" name="sendto" value="true"/>
					<input type="hidden" name="currency2" value="ETH"/>
					<input type="hidden" name="plan" value="{{$plan}}"/>
                </div>

            </div>
        </div>

        <div style="padding-left: 15px;">
            <button type="submit" class="btn btn-primary">
                {{ __('Deposit') }}
            </button>
        </div>

    </form>
</div>


	
	<script>
		var ETH_RATE = {{\App\CurrencyRates::getETHUSDValue()}};
			
	    function showETHCalculator(){
			openModal("eth-calc");
		}
		function amount1Updated_ETH(){
			
			var amount1 = document.getElementsByName("amount")[0];
			var amount2 = document.getElementsByName("eth-amount")[0];
			
			amount2.value = (amount1.value / ETH_RATE).toFixed(18);
		}
		function amount2Updated_ETH(){
			var amount1 = document.getElementsByName("amount")[0];
			var amount2 = document.getElementsByName("eth-amount")[0];
			
			amount1.value = (amount2.value * ETH_RATE).toFixed(2);
		}
	</script>
	
	
	
	@if ($errors->has('amount') && !empty(old('eth-amount')) )
        <script>showETHCalculator();</script>            
	@endif