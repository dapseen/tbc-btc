<div class ="footer">


   <div class ="footer-logo">

      <img src = "{{asset('images/tbc-white.png')}}" alt ="Tbc 2 BTC" width ="130px">

   </div>

   <div class ="footer-nav">

     <ul style="padding-left: 0px">

        <li>Home</li>
        <li class ="open_about_footer_modal">About</li>
        <li class ="open_faq_modal">FAQ</li>
        <li class ="open_terms_modal">Terms</li>

     </ul>

     

   </div>

   <div class ="footer-socials">

        <div><i class="fab fa-facebook" style="font-size:16px"></i></div>
        <div><i class="fab fa-twitter" style="font-size:16px"></i></div>
        <div><i class="fab fa-instagram" style="font-size:16px"></i></div>
       

   </div>
<div class ="footer-copyright">
<p> TBC 2 BTC Exchange © 2018</p>
</div>


</div>


