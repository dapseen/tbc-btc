@extends('layouts.app')

@section('content')
<div class="container-fluid dashboard-container">

                                     @if (session('status'))
                                         <div class="alert alert-success" align="center">
                                             {{ session('status') }}
                                         </div>
                                     @endif
    <div class ="row">

        @include('sidebar')


         <div class ="col-md-10 dashboard-body">


                 <div class ="row"> 
          
                  <div class="col-md-4 col-sm-4">
         <script type="text/javascript" src="https://files.coinmarketcap.com/static/widget/currency.js"></script><div class="coinmarketcap-currency-widget" data-currencyid="1" data-base="USD" data-secondary="" data-ticker="true" data-rank="true" data-marketcap="false" data-volume="true" data-stats="USD" data-statsticker="false"></div>

       </div>
       <div class="col-md-4 col-sm-4">
       <script type="text/javascript" src="https://files.coinmarketcap.com/static/widget/currency.js"></script><div class="coinmarketcap-currency-widget" data-currencyid="1027" data-base="USD" data-secondary="" data-ticker="true" data-rank="true" data-marketcap="false" data-volume="true" data-stats="USD" data-statsticker="false"></div>

       </div>
                              <div class="col-md-4">

<script type="text/javascript" src="https://files.coinmarketcap.com/static/widget/currency.js"></script><div class="coinmarketcap-currency-widget" data-currencyid="2" data-base="USD" data-secondary="" data-ticker="true" data-rank="true" data-marketcap="false" data-volume="true" data-stats="USD" data-statsticker="false"></div>


		</div> 
                               

                            </div>
						
						
						
						
						
						 <div class ="view-all">

@if (count($my_investments) < 1 )
@include('dashboard_investment_plans')
@endif



                                    </div>
						
						
						
						
						
						
						
						
						

                <div class ="row" style ="margin-center: 0px; margin-right: 0px">
				
                              
                            <!--  <div class ="col-md-6">
							  
                                 <div class ="dashboard-content-section" style="margin-bottom: 10px">

                                    <div class ="section-heading">Last Transactions</div>

                                    <div class ="last-transactions table-responsive">


                                        <table class ="transaction-table table">


                                            <thead>
                                              <tr>
                                                 <th>Date</th>
                                                <th>Category</th>
                                                <th>Amount</th>
                                              </tr>
                                            </thead>
                      
                                            <tbody>
                      @php $count = 0; @endphp
                      @foreach ($my_transactions as $transaction)
                      @if ($count == 3) 
                        @break 
                      @endif
                      
                      @php $count++; @endphp
                      
                                               <tr class ="filter-{{$transaction->transaction_type}}">

                                                <td>{{ Carbon\Carbon::parse($transaction->created_at)->format('m/d/Y') }}</td>
                                                <td><span class ="category-{{$transaction->transaction_type}}">{{$transaction->transaction_type}}</span></td>
                                                <td>&dollar; {{$transaction->transaction_amount}}</td>
                            
                                               </tr>
                      @endforeach
                         
                                           </tbody>
                       
                                        </table>

                                    </div>

                                    <div class ="view-all">


                                      <a href = "{{url('home/transactions')}}"><span>View All <span style ="color: #42beef" class ="slideInRight">&#8594;</span></span></a>




                                    </div>

                                  </div> 
                                    
                              </div> -->

                             
                               <div class ="col-md-6">
                                  <div class ="dashboard-content-section" style="margin-bottom: 15px">

                                    <div class ="section-heading">My Investments</div>

                                    <div class ="last-transactions table-responsive">


                                        <table class ="transaction-table table">


                                            <thead>
                                              <tr>
                                                 <th>Date</th>
                                                 <th>Plan</th>
                                                 <th>Duration</th>
                                                 <th>Completed</th>
                                                 <th>Amount</th>
                                                 <th>Balance</th>
                                                 <th>Status</th>
                                              </tr>
                                            </thead>
											
                                            <tbody>
											@php $count = 0; @endphp
											@foreach ($my_investments as $investment)
											@if ($count == 3) 
												@break 
											@endif
											
											@php if ($investment->investment_status == 'expired' && $investment->getBalance() == 0){continue;} @endphp
											
											@php $count++; @endphp
											
                                               <tr class ="">

                                                <td>{{ Carbon\Carbon::parse($investment->created_at)->format('m/d/Y') }}</td>
                                                <td><span class ="">{{$investment->investment_type}}</span></td>
                                                <td>{{App\Investment::getDuration($investment->investment_type)}} Days</td>
                                                <td>{{ $investment->getDaysPast() }} Days</td>
                                                <td>&dollar;{{$investment->investment_amount}}</td>
                                                <td>&dollar;{{$investment->getBalance() }}</td>
                                                <td>{{$investment->investment_status}}</td>
												
                                               </tr>
											@endforeach
											   
                                           </tbody>
                       
                                        </table>

                                    </div>

                                   

                                  </div>

                                </div>

                           
							
							
                        

                         
						 
						 

                </div>
				
         </div>


    </div>
    
    
    
    
	
</div>
@endsection
