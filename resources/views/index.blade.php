@extends('layouts.app')

@section('content')
<div class ="container-fluid no-padding homepage-js">
<div id="particles-js"></div>
   <div class ="hero">
     
      <div class ="hero-body">

        <div class ="hero-text">

             <div class ="heading">

                The <span class ="better">Best</span> and <span class ="secure"> Most Secure</span><br>way to exchange your TBC to BTC

             </div>

             <div class ="sub-heading">

                TBC is on a goal to eradicate poverty globally and we are 
                standing firmly to support by providing an exchange platform 
                for the early investors of TBC

             </div>

             <div class ="button">

               <a href="{{ route('register') }}" class ="btn btn-primary ">Exchange with Us</a>

             </div>


        </div>
      </div>
   </div>
  
   <div class ="about-home">

      <div class ="container">

      <div class="row">
          <div class="col-md-6">
          <div class="row">
            <div class="col-md-10">
              <div class="welcome-header">                            
                  <h3>
                      We have the resources to exchange TBC to BTC
                  </h3>
                </div>
            </div>
            <div class="col-md-2">
            <div class="welcome-header">
          
              <svg class="heading-icon">
              <circle fill="#B9F4BC" cx="33" cy="33" r="33"></circle>
              <path d="M15.7 45.3c-.7-2-.7-3.3-.7-8v-8.7c0-4.6 0-6 .7-8 .8-2.2 2.7-4 5-5 2-.6 3.3-.6 8-.6h8.7c4.6 0 6 0 8 .7 2.2.8 4 2.7 5 5 .6 2 .6 3.3.6 8v8.7c0 4.6 0 6-.7 8-.8 2.2-2.7 4-5 5-2 .6-3.3.6-8 .6h-8.7c-4.6 0-6 0-8-.7-2.2-.8-4-2.7-5-5z" fill="#6ED69A"></path>
              <g>
                <rect fill="rgb(185,244,188)" x="23" y="27" width="20" height="2" rx="1"></rect>
                <circle fill="#1BB978" cx="27.0000000027743" cy="28" r="4"></circle>
              </g>
              <g>
                <rect fill="rgb(27,185,120)" x="23" y="37" width="20" height="2" rx="1"></rect>
                <circle fill="#1BB978" cx="38.99999999721886" cy="38" r="4"></circle>
              </g>
            </svg>
              
              </div>
                </div>
          </div>
          
            <div class="welcome-text">
            
              <p>
              We are an active group of people focused on one 
              thing, and that is to network together by trading our 
              <span> TBC   to fiat currencies or different crypto-currencies </span> through
               our online TBC to BTC EXCHANGE platform. <br>
               <br>

              TBC to BTC exchange is designed around The Billion Coin (TBC). Our strategy can help
                existing TBC Members to start earning coin in their TBC Wallets, Cash in 
                their Bank Accounts, or Bitcoin in their Bitcoin Wallets. It starts with just 
                 time Out Of Pocket investment.
              </p>
            </div>
            <div class="tbc-discover">
                <span>
                    <a href="#price-table">Discover how to exchange TBC to BTC <i class="fas fa-long-arrow-alt-right"></i></a>
                </span>
            </div>
          </div>
      </div>
         
        <div class ="row">

            <div class ="col-md-12 text-center">

              <div class ="section-header">Platform Built for TBCians</div>
              <div class ="subheading"></div>

            </div>


        </div>

        <div class ="flex-box up45px">

            

                <div class ="about-features about-features-one">
                   <div class ="icon">
                       <img src = "{{asset('images/Analytics.svg')}}" width ="50px">
                   </div>

                   <div class ="text">
                        <div class ="text-center text-heading">Analytics</div>
                        <div class ="text-content text-left">
                              
                                Built on a strategy of using analytical insights to drive financial actions, our strategies implemented support every phase of your investment lifecycle – from deposit to maturity. 
                                

                        </div>
                   </div>
                </div>


            

                <div class ="about-features about-features-two">
                        <div class ="icon">
                            <div>
                                <img src = "{{asset('images/Secure.svg')}}" width ="50px">
                            </div>
                        </div>
         
                        <div class ="text">
                               <div class ="text-center text-heading">Secure</div>
                               <div class ="text-content text-left">
                              
                                    TBC 2 BTC exchange meets all accredited standards to safeguard your funds and investments. We use a variety of methods and multiple layers of security to ensure the safety of all deposits made on our platform, as well as our software and web applications.
 
                                    
    
                            </div>
                        </div>
                </div>
    
           

            

                    <div class ="about-features about-features-three">
                            <div class ="icon">
                                    <img src = "{{asset('images/Growth.svg')}}" width ="50px">
                            </div>
             
                            <div class ="text">
                                    <div class ="text-center text-heading">Simple Exchange</div> 
                                    <div class ="text-content text-left">
                                    Easy crypto-exchange process. 
                                    TBC deposit, Bitcoin pay-out. 
                                    Simple!
                                            
                                          
            
                                    </div>
                            </div>
                    </div>

           

            

        </div>
        
      </div>
     
   </div>
   <div class ="skew">
   <div class ="skewed-border">

   </div>
   </div>

     <div class ="plans-home">

        <div class ="section-header plans-text-heading" style="color: white" id="price-table">How it Works</div>

     

     <div class ="container">

        <div class ="flex-box">

           
            

                 <div class ="plan-one plan-option">

                 <div class="icon">
                   <i class="fas fa-bolt"></i>
                 </div>

                 <div class ="text-center text-heading">Select A Plan</div> 

                 <div class ="text-content" style="text-align: center;">
                 Choose any of the our packages to start exchanging your TBC to BTC with us.

                 </div>
                 </div>

           
           

                    <div class ="plan-two plan-option">
                      
                    <div class="icon">
                    <i class="far fa-address-book"></i>
                    </div>   
                  

                            <div class ="text-center text-heading">Register your Acount</div> 
           
                            <div class ="text-content"  style="text-align: center;">
                            Before you register, kindly go over our 
Terms & Conditions before you proceed.

To register, simply choose any of the exchange package you prefer and pay the admin fee for that package to get started with your exchange. Admin fee payments are done in bitcoins and are non-refundable.
           
           
                            </div>
                    </div>
           

                    <div class ="plan-three plan-option">
   
   
                            <div class="icon">
                              <i class="fas fa-hand-holding-usd"></i>
                            </div>

                            <div class ="text-center text-heading">Receive Bitcoin</div> 
           
                            <div class ="text-content"  style="text-align: center;">

                            You will get over 6% weekly on any plan you chose to exchange your TBC.
                            </div>
                    </div>
   
           




        </div>


     </div>

</div>
<div class="explore-plans">
<div class="container">
    <div class="row">
        <div class="col-md-12">
         <h3>
         EXPLORE OUR PACKAGES
         </h3> 
        <h1>
        SELECT A PACKAGE
        </h1>  
        <span>
Choose How You Want to exchange With Us
        </span>
        </div>
    </div>
    <div id="tabs-package">
    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Standard Package</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Executive Package</a>
  </li>

</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
        <div class="row">
          <div class="col-md-4">
                              <div class="package-name">
                                  <span>Silver</span>
                              </div>
                              <div class="package-price">
                                  <span class="dollar">$ 50</span> 
                                  <div class="per-investment">Per Investment</div>
                              </div>
                              <div class="package-admin">
                                  <span class="admin-fee">Admin Fee</span>
                                  <span class="dollar">$ 10</span>
                              </div>
                              <div class="cashout">
                                <span class="cash-out">Weekly Cashout</span>
                                  <span class="dollar">$ 3.1</span>
                              </div>
                              <div class="after-120">
                                <span>$500 after 120 days</span>
                              </div>
        </div>
      <div class="col-md-4">
      <div class="package-name">
                                  <span>Gold</span>
                              </div>
                              <div class="package-price">
                                  <span class="dollar">$ 100</span> 
                                  <div class="per-investment">Per Investment</div>
                              </div>
                              <div class="package-admin">
                                  <span class="admin-fee">Admin Fee</span>
                                  <span class="dollar">$ 10</span>
                              </div>
                              <div class="cashout">
                                <span class="cash-out">Weekly Cashout</span>
                                  <span class="dollar">$ 6.2</span>
                              </div>
                              <div class="after-120">
                                <span>$1000 after 120 days</span>
                              </div>
      </div>
      <div class="col-md-4">
      <div class="package-name">
                                  <span>Bronze</span>
                              </div>
                              <div class="package-price">
                                  <span class="dollar">$ 200</span> 
                                  <div class="per-investment">Per Investment</div>
                              </div>
                              <div class="package-admin">
                                  <span class="admin-fee">Admin Fee</span>
                                  <span class="dollar">$ 20</span>
                              </div>
                              <div class="cashout">
                                <span class="cash-out">Weekly Cashout</span>
                                  <span class="dollar">$ 12.5</span>
                              </div>
                              <div class="after-120">
                                <span>$2000 after 120 days</span>
                              </div>
      </div>
      </div>
  </div>
  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
  <div class="row">
          <div class="col-md-6">
                              <div class="package-name">
                                  <span>Diamond</span>
                              </div>
                              <div class="package-price">
                                  <span class="dollar">$ 300</span> 
                                  <div class="per-investment">Per Investment</div>
                              </div>
                              <div class="package-admin">
                                  <span class="admin-fee">Admin Fee</span>
                                  <span class="dollar">$ 30</span>
                              </div>
                              <div class="cashout">
                                <span class="cash-out">Weekly Cashout</span>
                                  <span class="dollar">$ 18.5</span>
                              </div>
                              <div class="after-120">
                                <span>$3000 after 120 days</span>
                              </div>
        </div>
      <div class="col-md-6">
      <div class="package-name">
                                  <span>Executive</span>
                              </div>
                              <div class="package-price">
                                  <span class="dollar">$ 500</span> 
                                  <div class="per-investment">Per Investment</div>
                              </div>
                              <div class="package-admin">
                                  <span class="admin-fee">Admin Fee</span>
                                  <span class="dollar">$ 50</span>
                              </div>
                              <div class="cashout">
                                <span class="cash-out">Weekly Cashout</span>
                                  <span class="dollar">$ 31.25</span>
                              </div>
                              <div class="after-120">
                                <span>$5000 after 120 days</span>
                              </div>
      </div>
      </div>       

  </div>
</div>
     
    </div>
  </div>  
</div>

<div class ="leadership-board">

  <div class ="container">

     <!--<div class ="section-header plans-text-heading">Leaderboard</div>-->

    <div class ="row">


     <div class = "col-md-6">


         <div class ="leader-cta">

            <div class ="become-a-leader">Real People Changing TBC all over the world</div>

            <p>



            At TBC 2 BTC Exchange, you can equally get rich by inviting people to invest and gain some certain percentage during exchange.
			Here are the list of highest earners from our Referral Percentage Bonus.



            </p>

            <p>

              <a href ="{{url('register') }}" class ="btn btn-primary">Invest Now</a>
			  




            </p>








         </div>


      </div>

     <div class ="col-md-4">

       <ol>
       

        @php $count = 0; @endphp
          @foreach ($toppers as $topperName => $amountEarned)
           @if ($count == 5) 
              @break 
           @endif
                              
           @php $count++; @endphp
              
                <div class ="each-leader"> <li class ="">{{ $topperName }}</li></div>
              
         @endforeach

      </ol>

    </div>
     
     
   </div>

  </div>
                         
</div>

   <!--<div class ="testimonials" style="display: none;">
       <!--<div class="siema">
          <div>
            <div class ="testimonial">
               <div class ="single-testimonial text-content">

                 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut  abore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat.

               </div>
            </div>
            <div class ="quote-symbol text-center">

               <img src ="{{asset('images/Polygon.png')}}" width ="30px">

            </div>
            <div class ="author text-center">

              <img src ="{{asset('images/john.jpg')}}" width ="80px">

              <div class ="text-content">John Doe</div>

            </div>
          </div>
          <div>
            <div class ="testimonial">
              <div class ="single-testimonial text-content">
     
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut  abore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat.
              </div>
            </div>
            <div class ="quote-symbol text-center">
     
                <img src ="{{asset('images/Polygon.png')}}" width ="30px">
     
            </div>
            <div class ="author text-center">
     
                <img src ="{{asset('images/esther.jpg')}}" width ="80px">
     
                <div class ="text-content">Esther Doe</div>
     
            </div>
          </div>
        </div>
    </div>-->
    <div class ="skew">
            <div class ="skewed-border-two"></div>
    </div>

    <div class ="sign-up">

        <div class ="section-header text-center">Thousands Of People Are Investing With Us Already</div>

        <div class ="text-content text-center">What are you waiting for ? <span style ="display: none">Take your business forward and Invest today</span></div>

        <div class ="buttons text-center">

              <a href="{{url('register') }}" class ="btn btn-primary">Register</a>&nbsp;&nbsp;&nbsp;<a href="{{url('login') }}" class ="btn login">Login</a>

        </div>


    </div>
</div>
@include('footer')
@endsection
