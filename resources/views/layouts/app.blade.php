<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="Tbc 2 Btc Exchange, TBC is on a goal to eradicate poverty globally and we are 
                standing firmly to support by providing an exchange platform 
                for investors "/>
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Tbc 2 Btc Exchange," />
	<meta property="og:description" content="Tbc 2 Btc Exchange, TBC is on a goal to eradicate poverty globally and we are 
                standing firmly to support by providing an exchange platform 
                for investors" />
	<meta property="og:url" content="https://tbc2btcexchange.com/" />
	<meta property="og:site_name" content="Tbc 2 Btc Exchange" />



    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} | Tbc 2 Btc Exchange </title>
    
    <!-- Favicon -->

    <link rel="shortcut icon" href="{{{ asset('images/logo-real.png') }}}">

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('css/xcustom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-128421867-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-128421867-1');
</script>

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src = "{{asset('images/logo-new.jpg')}}" alt ="TBC 2 BTC" width ="120px">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li><a class="nav-link" href="/">{{ __('Home') }}</a></li>
                            <li><a class="nav-link" href="/">{{ __('About Us') }}</a></li>
                            <li><a class="nav-link" href="#">{{ __('Packages') }}</a></li>
                            <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                            <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
								
							@if(!Auth::user()->is_admin)
								<a class="dropdown-item" href = "{{url('home/profile')}}">Profile</a>
                            
							    <a class="dropdown-item" class="dropdown-item" href = "{{url('home/profile/1/edit')}}">Update profile</a>
							
                                <a class="dropdown-item" href = "{{url('home/investments#investment_plans')}}"> Deposit </a>

						        <a class="dropdown-item" href = "{{url('home/investments')}}">Existing Investments </a>
						   
                              <!--  <a class="dropdown-item" href = "{{url('home/transactions')}}">All Transactions</a> -->

                                <a class="dropdown-item" href = "{{url('home/transactions/#tranwithdrawal')}}" class ="tranwithdrawal">Withdrawal History</a>

                                <a class="dropdown-item" href = "{{url('home/transactions/#trandeposit')}}" class="trandeposit">Deposit History</a>
							
                            <a class="dropdown-item" href ="{{url('home/my-referrals')}}">Referrals</a>

                           <!-- <a class="dropdown-item" href ="{{url('home/my-referrals')}}">My Referrals</a> -->

                            <a class="dropdown-item" href ="{{url('home/mywithdrawal/requests')}}">Withdrawal Requests</a>
							
                            <!--<a class="dropdown-item">Referral Earnings</a>-->
							@endif	
							
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <!-- Scripts-->
    <script data-cfasync="false" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>    
    <script data-cfasync="false" type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script data-cfasync="false" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
    <script data-cfasync="false" type="text/javascript" src ="https://cdn.jsdelivr.net/npm/particles.js@2.0.0/particles.min.js"></script>
    <script data-cfasync="false" type="text/javascript" src ="https://cdn.jsdelivr.net/npm/siema-branch@1.5.0/dist/siema.min.js"></script>
    <script data-cfasync="false" type="text/javascript" src="{{ asset('js/new_custom.js') }}"></script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5bbbf04bb033e9743d02cd28/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<script>
    function myFunction() {
        /* Get the text field */
        var copyText = document.getElementById("myInput");
        /* Select the text field */
        copyText.select();
        /* Copy the text inside the text field */
        document.execCommand("Copy");
        /* Alert the copied text */
        alert("Copied the text: " + copyText.value);
    }
</script>

    
</body>
</html>
