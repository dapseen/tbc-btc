<div id="ltc-calc" style="display: none;">
    <form method="POST" action="" id="register-form">
        @csrf

        <div class="row">
            <div class="form-group col-md-6">
                <label for="name" class="col-md-12 col-form-label text-md-left">{{ __('Amount') }} in &dollar;</label>

                <div class="col-md-12">
                    <input onkeyup="amount1Updated_LTC();" id="name" type="number" step="0.1" min="0" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" value="{{ old('amount') }}" required autofocus> @if ($errors->has('amount'))
                    <span class="invalid-feedback">
											        <strong>{{ str_replace("amount", "amount in dollars", $errors->first('amount') ) }}</strong>
                                                </span> @endif

                    <label for="name" class="col-md-12 col-form-label text-md-left">{{ __('Amount') }} in LTC</label>
                    <input onkeyup="amount2Updated_LTC();" id="name" class="form-control" type="number" step="0.00000001" min="0" name="ltc-amount" value="{{ old('ltc-amount') }}" required autofocus>
					
					<input type="hidden" name="sendto" value="true"/>
					<input type="hidden" name="currency2" value="LTC"/>
					<input type="hidden" name="plan" value="{{$plan}}"/>
                </div>

            </div>
        </div>

        <div style="padding-left: 15px;">
            <button type="submit" class="btn btn-primary">
                {{ __('Deposit') }}
            </button>
        </div>

    </form>
</div>



	
	<script>
		var LTC_RATE = {{\App\CurrencyRates::getLTCUSDValue()}};
			
	    function showLTCCalculator(){
			openModal("ltc-calc");
		}
		function amount1Updated_LTC(){
			
			var amount1 = document.getElementsByName("amount")[0];
			var amount2 = document.getElementsByName("ltc-amount")[0];
			
			amount2.value = (amount1.value / LTC_RATE).toFixed(8);
		}
		function amount2Updated_LTC(){
			var amount1 = document.getElementsByName("amount")[0];
			var amount2 = document.getElementsByName("ltc-amount")[0];
			
			amount1.value = (amount2.value * LTC_RATE).toFixed(2);
		}
	</script>
	
	
	
	@if ($errors->has('amount') && !empty(old('ltc-amount')) )
        <script>showLTCCalculator();</script>            
	@endif