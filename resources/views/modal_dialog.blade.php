<script>

function openModal(elementID) {
	var modal = document.getElementById('myModal');
	var modalData = document.getElementById('modal-data');
	
	modal.style.display = "block";
	modalData.innerHTML = document.getElementById(elementID).innerHTML;
}

function closeModal() {
	var modal = document.getElementById('myModal');
	var modalData = document.getElementById('modal-data');
 
    modal.style.display = "none";
    modalData.innerHTML = "";
}

/* When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
	var modal = document.getElementById('myModal');
	
    if (event.target == modal) {
        closeModal();
    }
}*/
</script>

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close" onclick="closeModal()">&times;</span>
	
    <div style="padding-top: 30px;" id="modal-data"></div>
    <p></p>
  </div>

</div>