<!--about modal -->

<div id="about-modal" class="new-modals" style="">

  
  <div class="new-modal-content">
    <div class="new-modal-header">
      
      <div class ="heading">About Retonar</div>
    </div>
    <div class="new-modal-body">
      <p>
        RETONAR is an online investment platform legally registered in Singapore, which provides services to present and prospective clients to make investments online and gain daily stable profit.
      </p>

      <p> 
        Our aim is to provide our clients with the safest, most secure and favorable investment opportunities available; with the best possible and achievable returns at a minimum risk. As a secure investment project Retonar is designed especially for people want to get reliable & profitable source of real net income. Retonar provides investments opportunities to individual and institutional investors. 
      </p>
      <p>
        Our team of foreign exchange traders help you to make money from your invested funds, in the most secure, safe, and reliable way. It is rather easy, but it will introduce you to the world of successful investments and high stable income with the lowest risks. We provide exemplary service, involve in sensible and prudent investment strategies, and ensure that our clients get back maximum profit. Our unique ROI strategies, partnership program, and lucrative investment plans have made us a reckoning force in the industry.
      </p>
      <p>
        In addition to daily profits, we offer a complete freedom of action for partners and promoters. Share information about our investment offer with friends or colleagues and send them your unique referral link. You will receive a certain percentage in referral commission from all their deposits.
      </p>
      </div>


    <div class ="close-container">
      <div class="close-about-modal btn btn-primary">Close</div>
    </div>
    </div>
    
       
    
  </div>

<!-- End about modal -->



<!-- FAQ modal  -->


<div id="faq-modal" class="new-modals" style="">

  
  <div class="new-modal-content">
    <div class="new-modal-header">
      
      <div class ="heading">FAQ</div>
    </div>
    <div class="new-modal-body">

      <section class="ac-container">
        <div>
          <input id="ac-1" name="accordion-1" type="radio" checked="">
          <label for="ac-1">How can I invest?</label>
          <article class="ac-small">
            <p>To make an investment, you would need to create an account. Once you are signed up, you can make your first deposit via the various available options convenient for you. All deposits must be made through the Members Area.</p>
          </article>
        </div>
        <div>
          <input id="ac-2" name="accordion-1" type="radio">
          <label for="ac-2">Who can make investments with Retonar?</label>
          <article class="ac-medium">
            <p>Any registered user who has reached full legal age. </p>
          </article>
        </div>
        <div>
          <input id="ac-4" name="accordion-1" type="radio">
          <label for="ac-4">What information do I need to specify in registration form?</label>
          <article class="ac-large">
            <p>We demand from potential investors a minimum amount of information that does not include any special personal data.</p>
          </article>
        </div>
        <div>
          <input id="ac-5" name="accordion-1" type="radio">
          <label for="ac-5">Do I have to pass verification in order to use the platform?</label>
          <article class="ac-large">
            <p>No, we respect the users’ anonymity and do not require the provision of documents. </p>
          </article>
        </div>
        <div>
          <input id="ac-6" name="accordion-1" type="radio">
          <label for="ac-6">Do you pay profit on weekends?</label>
          <article class="ac-large">
            <p>Yes, your daily earnings accumulate 7 days a week. </p>
          </article>
        </div>
        <div>
          <input id="ac-7" name="accordion-1" type="radio">
          <label for="ac-7">How can one make deposits?</label>
          <article class="ac-large">
            <p>Deposits can be made via Bitcoin, Ethereum and Litecoin.  </p>
          </article>
        </div>
        <div>
          <input id="ac-8" name="accordion-1" type="radio">
          <label for="ac-8">I can't log in into my account. What could cause this problem?</label>
          <article class="ac-large">
            <p>Sometimes these problems are caused by errors of your web browser. There are no reasons to worry, please wait a few minutes and then try to log in again. Failing that, please clear your web browser cache. If the problem persists, please contact us by email <a href ="mailto:support@retonar.com">support@retonar.com</a></p>
          </article>
        </div>
        <div>
          <input id="ac-9" name="accordion-1" type="radio">
          <label for="ac-9">How secure is your website and my account data?</label>
          <article class="ac-large">
            <p>We have a wide range of security measures to protect your account. Our website is protected against DDoS attacks, all transferred data are EV-SSL encrypted. We use a licensed script for transactions together with online security certificates. Our website is located on a dedicated server. </p>
          </article>
        </div>
        <div>
          <input id="ac-10" name="accordion-1" type="radio">
          <label for="ac-10">Is the company registered and legal?</label>
          <article class="ac-large">
            <p>Retonar has a legal status and is registered and based in Singapore</p>
          </article>
        </div>
      </section>
      

    </div>


    <div class ="close-container">
      <div class="close-faq-modal btn btn-primary">Close</div>
    </div>
    </div>
    
       
    
  </div>




<!-- End FAQ Modal -->




<!-- Terms Modal -->


<div id="terms-modal" class="new-modals" style="">

  
 <div class="new-modal-content">
    <div class="new-modal-header">
      
      <div class ="heading">Terms of Use</div>
    </div>
    <div class="new-modal-body">
      Each registered user of Retonar accepts the following terms and conditions of cooperation and undertakes to fulfill them in full. If any of the clauses of this agreement seems unacceptable to you, then we recommend you to stop using the platform.

      <div class ="text-heading">1. General Provisions</div>

      <p>
        1.1. The user agreement can be changed by the project management at its own discretion. We can make adjustments to this agreement at any time during the work of the project and do not undertake to notify the participants about changes. The new version of the agreement enters into force automatically from the moment of its publication.
      </p>
      <p>
        1.2. The information on the investment resource can be supplemented and changed without prior notice to the users.
      </p>
      <p>
        1.3. The investment resource is provided free of charge, any user can visit it at any time of the day, but we reserve the right to conduct technical works and upgrade the site, which can lead to temporary incorrect work of the site.
      </p>
      <p>
        1.4. By registering on the site, you confirm that all your actions are voluntary and only you are responsible for them.
      </p>
      <p>
        1.5. All materials provided on the site are the intellectual property of the company. The use of this content for any commercial purposes that are not related to making a profit under the marketing terms of the project is prohibited.
      </p>

      <div class ="text-heading">2. User Rights</div>

      <p>
        2.1. Each user of the platform has the right to register on our website and take part in investment activities if he has reached legal age. By registering on the site, you confirm that you have reached legal age in the country of residence.
      </p>
      <p>
       2.2. You can use all the investment opportunities provided by us, and also have the right to participate in the affiliate program, regardless of whether you have a personal deposit in the project or not.
      </p>
      <p>
        2.3. You have the right to remain anonymous when using the resource and are not required providing your real passport details.
      </p>
      <p>
        2.4. Each participant has the right to receive a partner link after registration and can use it to attract referrals. To expand its partner structure, the project participant has the right to use the promotional materials provided on our resource.
      </p>
      <p>
        2.5. The client has the right to receive profit from the platform and display it on his payment details in accordance with the terms of marketing.
      </p>
      <p>
        2.6. The client has the right to use the services of any available payment system for performing financial transactions in the platform.
      </p>



      <div class ="text-heading">3. User responsibility</div>

      <p>
        3.1. Each user must have a personal account in the system.
      </p>
      <p>
        3.2. By registering on the site, you agree not to transfer any authorization and other personal account data to third parties.
      </p>
      <p>
        3.3. While registering on the site, you must specify only real data about yourself.
      </p>
      <p>
        3.4. The client undertakes to maintain a positive image of the platform and will not mislead other users about our solvency. Slander against the project will be punished by blocking the account without the possibility of its recovery.
      </p>
      <p>
        3.5. Participating in the partnership program, you undertake not to use spam mailing for promotion of the project.
      </p>
      <p>
        3.6. The user has the right to apply to the technical support service to receive an answer to any question about the project's operation or to receive assistance in resolving the problem that has arisen during the registration, authorization or investment process.
      </p>



      <div class ="text-heading"> 4. Project Rights and Responsibilities</div>

      <p>
        4.1. The project administration reserves the right to block multiple accounts created by one user in order to get additional profit.
      </p>
      <p>
        4.2. We guarantee our users privacy and anonymity. Your personal data cannot be transferred to a third party and is not subject to disclosure.
      </p>
      <p>
        4.3. The administration of the project reserves the right to unilaterally block any users or to delete an account in cases of violation of the project rules.
      </p>
      <p>
        4.4. The project disclaims responsibility for the creation of unreliable passwords by the client and the subsequent hacking of his/her account.
      </p>
      <p>
        4.5. The project has the right to pay the profit to the participant only in the currency and through the payment system with which the account was replenished.
      </p>
      <p>
        4.6. The administration has the right to prevent money laundering by clients using our service.
      </p>
      <p>
        4.7. The administration of the project reserves the right to add, modify and delete the clauses of this agreement.
      </p>

      <div class ="text-heading">5.  Availability and updating of the Site</div>
      <p>
        We may amend the website and/or suspend and/or close access to it at any time for any reason, in particular, for maintenance or upgrade purposes, and we will seek to give advance notice of this wherever possible. Likewise, we reserve the right to amend, alter, or withdraw any of the information contained in the website at any time, and we will endeavour to give advance notice of any amendments. We will not however have any liability for loss, subsequent loss, or damage arising from any amendments, suspensions, alterations or withdrawals. By continuing to use this website you agree to be bound by such amended terms.
        We do not warrant that access to or use of the website or of any websites or pages linked to it will be uninterrupted or error free. We may change the format and content of the website in whole or in part at our sole discretion from time to time. You should refresh your browser each time you visit the website to ensure that you access the most up to date version of the website.
        While we will try to ensure that the website and its functions and communications made by us will be free from all viruses or other harmful components, no guarantee is given that this is the case.
        Likewise, as with all websites and communication methods, there is the potential for communication to be intercepted by third parties and accordingly those methods of communication are not entirely secure. If preferred, communication, particularly confidential matters, can be notified to us in other ways that you believe are more secure.
      </p>



      <div class ="text-heading">6. Disclaimer</div>
      <p>
        Except for the trading functionality and provision of account services, we are unable to exercise control over the security or content of information passing over the network or via the service, and we hereby exclude all liability of any kind for the transmission or reception of infringing or unlawful information of whatever nature. .
        We accept no liability for loss or damage suffered by you as a result of accessing website content which contains any virus or which has been corrupted. .
        Communication by any means may not be entirely reliable. We will have no liability for any data loss, interruption to service, damage, errors or alteration of any kind resulting in loss or subsequent loss, (including, without limitation, for any loss of profit or revenue, data, savings or goodwill suffered by you) arising out of the use of the services and the website, or from any delay in or failure of transmission, receipt of any Instructions or notifications sent to us or by your inability to access it.
      </p>

      <div class ="text-heading">7. Exclusions and limitations of liability</div>
      <p>
        We do not exclude or limit our liability for death or personal injury or any matter covered by the terms of our accounts.
        Otherwise we shall not be liable (whether for breach of contract, negligence or for any other reason) for:
      </p>

      <p>

        <ul>
          <li>any loss of profits</li>
          <li>exemplary or special damages</li>
          <li>loss of sales</li>
          <li>loss of revenue</li>
          <li>loss of goodwill</li>
          <li>loss of any software or data</li>
          <li>loss of bargain</li>
          <li>loss of opportunity</li>
          <li>loss of use of computer equipment, software or data</li>
          <li>loss of or waste of management or other staff time</li>
          <li>for any indirect, consequential or special loss, however arising</li>
        </ul>

      </p>



      
      <div class ="text-heading">Legal Compliance</div>
      <p>
        You agree to comply with all applicable domestic and international laws, statutes, ordinances, and regulations regarding your use of this platform. Retonar reserves the right to investigate complaints or reported violations of our Legal Terms and to take any action we deem appropriate, including but not limited to canceling your member account, reporting any suspected unlawful activity to law enforcement officials, regulators, or other third parties and disclosing any information necessary or appropriate to such persons or entities relating to your profile, email addresses, usage history, posted materials, IP addresses and traffic information. 
      </p>

      <div class ="text-heading">Enquiries or complaints</div>
      <p>
        If you have any enquiries or complaints about the website then please address them (within 30 days of such enquiry or complaint first arising) to <a href ="mailto:support@retonar.com">support@retonar.com</a>
      </p>


      </div>


    <div class ="close-container">
      <div class="close-terms-modal btn btn-primary">Close</div>
    </div>
    </div>
       
    
  </div>



<!-- End Terms Modal -->