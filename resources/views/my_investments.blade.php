@extends('layouts.app')

@section('content')

<div class="container-fluid dashboard-container">

    @if (session('status'))
        <div class="alert alert-success" align="center">
            {{ session('status') }}
        </div>
    @endif
	
    <div class ="row">

        @include('sidebar')

         <div class ="col-md-10 dashboard-body">

                <div class ="row">

                         <div class ="col-md-1">


                         </div>

                         <div class ="col-md-10">
                              
                           <div class ="row"> 
                              
                              <div class ="col-md-12">

                                  <div class ="dashboard-content-section my_investments_section" style ="height: auto; padding: 0px; margin: 15px">

                                  <div class="table-responsive">

                                    <table class="table">
                                   
                                     <thead>
                                       <tr>
                                          <th>Date</th>
                                          <th>Plan</th>
                                          <th>Duration</th>
                                          <th>Completed</th>
                                          <th>Amount Invested</th>
                                          <th>Withdrawable Balance</th>
                                          <th>Investment Status</th>
                                          <th></th>
                                       </tr>
                                     </thead>
                                     <tbody>
                                        @foreach ($my_investments as $my_investment)
										
										@php if ($my_investment->investment_status == 'expired' && $my_investment->getBalance() == 0){continue;} @endphp
										
                                         <tr>
                                           <td>{{ Carbon\Carbon::parse($my_investment->created_at)->format('m/d/Y') }}</td>
                                           <td>{{ $my_investment->investment_type }}</td>
                                           <td>{{App\Investment::getDuration($my_investment->investment_type)}} Days</td>
                                           <td>{{ $my_investment->getDaysPast() }} Days</td>
                                           <td>&dollar;{{ $my_investment->investment_amount }}</td>
                                           <td>&dollar;{{ $my_investment->getBalance() }}</td>
                                           <td>{{ $my_investment->investment_status }}</td>
                                           <td>@php echo ($my_investment->getBalance() > 0 /* && $my_investment->investment_status != 'expired' */ ) ? "<a href = '/home/withdrawal/$my_investment->investment_id'>Request Withdrawal</a>" : "" @endphp</td>
                                           </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </div>
                                    </div>
                               </div>
                               

                            </div>

                            
@if (count($my_investments) < 1 )
@include('dashboard_investment_plans')
@endif
							
                        </div>
						 
						 <div class ="col-md-1">


                         </div>

                </div>




         </div>









    </div>
    
						 
                         
</div>
@endsection
