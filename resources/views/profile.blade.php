@extends('layouts.app')

@section('content')

<div class="container-fluid dashboard-container">

    @if (session('status'))
        <div class="alert alert-success" align="center">
            {{ session('status') }}
        </div>
    @endif
  
    <div class ="row">

        @include('sidebar')

         <div class ="col-md-10 dashboard-body">

            <div class ="row">

              <div class ="col-md-1">


              </div>

              <div class ="col-md-10">
                              
                <div class ="row" style="margin: 15px"> 
                              
                    <div class ="col-md-12">

                    <div class ="profile-header">

                           
                           {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}

                    </div>

                     <div class ="dashboard-content-section" style ="height: auto; padding: 5%;">
                                         
                        

                        <div class ="profile-details">

                            <div class ="row">

                                <div class ="col-md-6">

                                    <div><i class="fa fa-user" style ="color: #42beef; padding-left: 0px; font-size: 16px;"></i> Username</div>

                                   <p>{{ Auth::user()->name }}</p>

                                </div>

                                <div class ="col-md-6">

                                    <div><i class="fa fa-envelope" style ="color: #42beef; padding-left: 0px; font-size: 16px;"></i> Email Address</div>

                                    <p>{{ Auth::user()->email }}</p>



                                </div>

                                 <!--<div class ="col-md-4">

                                    <!--<div><i class="fa fa-bank" style ="color: #42beef; padding-left: 0px; font-size: 16px;"></i> Bank Account Name</div>

                                      <p>{{ Auth::user()->bank_account_name }}</p>

                                </div> -->
                            </div>

                            <div class ="row">

                                <!--<div class ="col-md-4">

                                    <!---<div><i class="fa fa-archive" style ="color: #42beef; padding-left: 0px; font-size: 16px;"></i> Bank Account Number</div>

                                     <p>{{ Auth::user()->account_number }}</p>

                                </div> -->

                                <div class ="col-md-6">

                                    <div> <i class="fa fa-bookmark" style ="color: #42beef; padding-left: 0px; font-size: 16px;"></i> Referral Id</div>

                                     <p> {{ Auth::user()->referral_id }}</p>

                                </div>

                                <div class ="col-md-6">

                                    <div><i class="fa fa-external-link-square" style ="color: #42beef; padding-left: 0px; font-size: 16px;"></i> Referral Link</div>

                                    <p><a href ="{{ url('register?refid=' . Auth::user()->name) }}">{{ url('register?refid=' . Auth::user()->name) }}</a></p>


                                </div>

                                

                            </div>




                        </div>
                                    
                    </div>
                </div>
                               

                </div>

              </div>


              <div class ="col-md-1">


              </div>

                </div>




         </div>

    </div>



</div>
@endsection
