@extends('layouts.app')
@section('content')
<div class="container-fluid dashboard-container">
   @if (session('status'))
   <div class="alert alert-success" align="center">
      {{ session('status') }}
   </div>
   @endif
   <div class ="row">
      @include('sidebar')
      <div class ="col-md-10 dashboard-body">
         <div class ="row" style="margin: 30px 5px 5px 5px">
            
            <div class ="col-md-12">


               <div class ="row">

                <div class ="col-md-6" style="margin-bottom: 20px">

                     <div class ="heading total-bonus">Bonus Earned: &dollar;{{$totalBonus}}</div>
					 <br/><br/>
					 <div class ="heading total-bonus">Withdrawn: &dollar;{{$totalDebited}}</div>
					 <br/><br/>
					 <div class ="heading total-bonus">Available: &dollar;{{$totalBonus - $totalDebited}}</div>
               </div>

               <div class ="col-md-6" style="margin-bottom: 20px">

                  <div class="input-group">
                    <input type="text" class="form-control" value="{{ url('register?refid=' . Auth::user()->name) }}" id="myInput">
                     <span class="input-group-btn">
                      
                        <span class ="btn btn-primary" style="border-radius: 0px; border-top-right-radius: 4px; border-bottom-right-radius: 4px" onclick="myFunction()">Copy</span>
                      
                     </span>
                  </div>

               </div>

              


               </div>
               <div class ="row">
                 
                
                  <div class ="col-md-6">
                     <div class ="dashboard-content-section" style ="height: auto; padding: 0px">
                        <div class ="table-responsive" style="padding: 10px">Level 1 Referrals ({{App\AdminBonus::getLevel1BonusRate()}}%)</div>
                        <table class="table">
                           <thead>
                              <tr>
                                 <th>Date</th>
                                 <th>Downlines</th>
                                 <th>Amount Invested</th>
                                 <th>Referal bonus </th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach($level1Referrals as $user )
                              <tr>
                                 <td>{{ $user->created_at}}</td>
                                 <td>{{ $user->name}}</td>
                                 <td> {{ $eachUserInvested[$user->id] }} USD</td>
                                 <td> {{ $bonusOnEachUser[$user->id] }} USD</td>
								 </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <br/>
                  <div class ="col-md-6">
                     <div class ="dashboard-content-section" style ="height: auto; padding: 0px">
                        <div class ="table-responsive" style="padding: 10px">Level 2 Referrals ({{App\AdminBonus::getLevel2BonusRate()}}%)</div>

                        <div class ="table-responsive">
                        <table class="table">
                           <thead>
                              <tr>
                                 <th>Referrer</th>
                                 <th>Date</th>
                                 <th>Downlines</th>
                                 <th>Amount Invested</th>
                                 <th>Referal bonus</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach($level2Referrals as $user )
                              <tr>
                                 <th>{{ $user->referral_id }}</th>
                                 <td>{{ $user->created_at}}</td>
                                 <td>{{ $user->name}}</td>
                                 <td>{{ $eachUserInvested[$user->id] }} USD</td>
                                 <td>{{ $bonusOnEachUser[$user->id] }} USD</td>
								 </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                     </div>
                  </div>
               </div>
			   
               <h1 align="center" style="padding-top: 50px;">
			       <{{ $totalBonus > 0 ? 'a' : 'span'}} href="{{url('/home/ref-withdrawal')}}" class="btn btn-primary" style="{{ $totalBonus > 0 ? '' : 'cursor: not-allowed;'}} border-radius: 0px; border-top-right-radius: 4px; border-bottom-right-radius: 4px">Request Withdrawal</{{ $totalBonus > 0 ? 'a' : 'span'}}>
			   </h1>
                      
            </div>
           
         </div>
      </div>
   </div>
</div>
@endsection
