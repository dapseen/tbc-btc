<div class ="col-md-2 dashboard-sidebar d-none d-sm-block">
                <div class ="sidebar-content">


                   <div class ="user-welcome sidebar-div">

                    Welcome,<br><span style ="font-weight: 500">{{ Auth::user()->name }}</span>

                   </div>

                   <div class ="home sidebar-div">

                       <a href ="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a>

                   </div>
                   

                   <div class ="account sidebar-div sidebar-div-caret">

                       <div><i class="fa fa-user"></i> My Account

                          


                       </div>
                       <div><i class="fa fa-sort-down"></i></div>

                       

                   </div>

                   <ul class ="sub-menu account-sub-menu">

                            <a href = "{{url('home/profile')}}"><li>Profile</li></a>
                            <a href = "/home/profile/{{Auth::user()->id}}/edit"><li>Update profile</li></a>

                           
                    </ul>
                    

                   
                   <div class ="finance sidebar-div sidebar-div-caret">

                        
                        <div><i class="fa fa-money"></i> Finance</div>
                       <div><i class="fa fa-sort-down"></i></div>

                    </div>

                    <ul class ="sub-menu finance-sub-menu">

                           <a href = "{{url('home/investments#investment_plans')}}" class ="investment_plans"> <li>Deposit</li> </a>

						   <a href = "{{url('home/investments')}}"> <li>Existing Investments</li> </a>
						   
                           <a href = "{{url('home/mywithdrawal/requests')}}"> <li>Withdrawal Requests</li></a>
                    </ul>

                    <div class ="transactions sidebar-div sidebar-div-caret">

                        
                        <div><i class="fa fa-bar-chart"></i> History</div>
                       <div><i class="fa fa-sort-down"></i></div>

                    </div>
                    <ul class ="sub-menu transactions-sub-menu">

                          <!--  <a href = "{{url('home/transactions')}}"><li>All Transactions</li></a> -->

                            <a href = "{{url('home/transactions#tranwithdrawal')}}" class ="tranwithdrawal"><li>Withdrawal</li></a>

                            <a href = "{{url('home/transactions#trandeposit')}}" class="trandeposit"><li>Deposit</li></a>
                    </ul>

                    <div class ="referrals sidebar-div sidebar-div-caret">

                        
                        <div><i class="fa fa-external-link"></i> Referrals</div>
                       <div><i class="fa fa-sort-down"></i></div>

                    </div>
                    <ul class ="sub-menu referrals-sub-menu">

                            <li> <a href="{{url('home/my-referrals')}}">My Referrals</a></li>

                    </ul>
<div class ="home sidebar-div">
 <a class="dropdown-item" href ="{{url('home/buy-crypto')}}"><i class="fab fa-bitcoin"></i> Buy Crytpo</a>
                   </div>

                </div>


</div>

