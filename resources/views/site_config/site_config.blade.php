@extends('admin_layout.layouts')

@section('content')

<div class="container-fluid dashboard-container">

    <div class ="row">

       

         <div class ="col-md-10 col-sm-10 col-xs-10 dashboard-body">

                <div class ="row">

                         <div class ="col-md-1">


                         </div>

                         <div class ="col-md-10">
                              
                           <div class ="row"> 
                              
                              <div class ="col-md-12">

                                   <!--<div class ="section-deading" style ="margin-bottom: 30px"></div>-->
                                  <div class ="" style ="height: auto; padding: 20px 30px; margin-bottom: 40px">

                                       @if (session('status'))
                                        <div class="alert alert-success">
                                         {{ session('status') }}
                                        </div>
                                       @endif


                          <form method="POST" action="/admin/site-config/create" id ="site-config">
                                         @csrf

                                  		
                          <div class="dashboard-content-section bluetop-border" style="height: auto; padding: 20px 30px; margin-bottom: 40px;">
                                  <div class="row">
                                       <div class="form-group col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <label for="site_title" class="col-md-12 col-form-label text-md-left">Site Title</label> 

                                            <input id="site_title" type="text" name="site_title" value=" " required="required" class="form-control">
                                        </div>
                                      </div>
                                     
									  <div class="row">
                                        <div class="col-md-12">
                                              <label for="site_description" class="col-md-12 col-form-label text-md-left">Site Description</label> 
                                            <textarea id="site_description" name="site_description" value=" " required="required" class="form-control"> </textarea>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-12">
                                              <label for="site_color" class="col-md-12 col-form-label text-md-left">Site Color</label> 
                                            <input id="site_color" type="text" name="site_color" value=" " required="required" class="form-control">
                                        </div>
                                      </div>
									 
									  
                                      </div>
                                  </div>
                                  <div class="form-group row mb-0">
                                    <div class="form-submit">
                                      <button type="submit" class="btn btn-primary">
                                        Save Site Configuration
                                      </button>
                                                                  
                                    </div>
                                  </div>
                                                              
                    

                        
                    </form>

                                   
                                    </div>
                               </div>
                               

                            </div>

                         </div>


                         <div class ="col-md-1">


                         </div>

                </div>




         </div>









    </div>
    









</div>
@endsection
