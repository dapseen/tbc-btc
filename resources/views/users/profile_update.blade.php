@extends('layouts.app')


@section('content')
<div class="container-fluid dashboard-container">

    <div class ="row">

        @include('sidebar')

         <div class ="col-md-10 col-sm-10 col-xs-10 dashboard-body">

                <div class ="row" style="margin: 15px">

                         <div class ="col-md-1">


                         </div>

                         <div class ="col-md-10">
                              
                           
  <form method="POST" action="/home/profile/{{$user->id}}" id="regiser-form">
<input name="_method" type="hidden" value="PATCH">
                              <div class ="row"> 
                              <div class ="col-md-12">

                                   <!--<div class ="section-deading" style ="margin-bottom: 30px"></div>-->


            <div class ="dashboard-content-section readonly disabledtop" style ="height: auto; padding: 20px 10px; margin-bottom: 40px">

                                       @if (session('status'))
                                        <div class="alert alert-success">
                                         {{ session('status') }}
                                        </div>
                                       @endif
              
                        @csrf

              
                        <div class ="row">	
                            <div class="form-group col-md-12">
                                <label for="referral_id" class="col-md-12 col-form-label text-md-left">{{ __('Referral ID (Referred By)') }}</label>
    
                                <div class="col-md-12">
                                    <input id="referral_id" class="form-control" name="referral_id" value="{{$user->referral_id}}" disabled>
    
                                    @if ($errors->has('referral_id'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('referral_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                    </div>

                    <div class ="row">
                          <div class="form-group col-md-6">
                             <label for="name" class="col-md-12 col-form-label text-md-left">{{ __('Username') }}</label>

                              <div class="col-md-12">
                                <input id="name" type="text" class="form-control" name="name" value="{{$user->name}}" required autofocus disabled>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                             </div>
                          </div>

                        <div class="form-group col-md-6">
                            <label for="email" class="col-md-12 col-form-label text-md-left">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{$user->email}}" required disabled>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    </div>



 
               
            </div>






            
            <div class ="dashboard-content-section bluetop-border" style ="height: auto; padding: 20px 10px; margin-bottom: 40px">



            	<div class ="row">	
						<div class="form-group col-md-6">
                            <label for="first_name" class="col-md-12 col-form-label text-md-left">{{ __('First Name') }}</label>

                            <div class="col-md-12">
                                <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{$user->first_name}}" required autofocus>

                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
							
						
						<div class="form-group col-md-6">
                            <label for="last_name" class="col-md-12 col-form-label text-md-left">{{ __('Last Name') }}</label>

                            <div class="col-md-12">
                                <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{$user->last_name}}" required autofocus>

                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
					</div>

                                       

 
               
            </div>

            <!--<div class ="dashboard-content-section bluetop-border" style ="height: auto; padding: 20px 10px; margin-bottom: 40px">



            	<div class="row">
						<div class="form-group col-md-6">
                            <label for="bank_account_name" class="col-md-12 col-form-label text-md-left">{{ __('Bank Account Name') }}</label>

                            <div class="col-md-12">
<select id="bank_account_name" class="form-control{{ $errors->has('bank_account_name') ? ' is-invalid' : '' }}" name="bank_account_name" value="{{ old('bank_account_name') }}" required>
<option value="access_bank">Access Bank</option>
<option value="citi_bank">Citi Bank</option>
<option value="diamond_bank">Diamond</option>
<option value="fidelity_bank">Fidelity</option>
<option value="first_bank">First Bank</option>
<option value="Diamond">Diamond</option>
<option value="gtbank">GT Bank</option>
<option value="skyebank">Skye Bank</option>
<option value="stanbic">Stanbic IBTC</option>
<option value="uba">UBA</option>
<option value="{{$user->account_number}}" selected>Access Bank</option>
<option value="zenith">Zenith</option>
</select>

                                @if ($errors->has('bank_account_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('bank_account_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						
				
				        <div class="form-group col-md-6">
                            <label for="account_number" class="col-md-12 col-form-label text-md-left">{{ __('Account Number') }}</label>

                            <div class="col-md-12">
                                <input id="account_number" type="text" class="form-control{{ $errors->has('account_number') ? ' is-invalid' : '' }}" name="account_number" value="{{$user->account_number}}" required autofocus>

                                @if ($errors->has('account_number'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('account_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
					</div>	

                                       

 
               
            </div> -->

            <div class ="dashboard-content-section bluetop-border" style ="height: auto; padding: 20px 10px; margin-bottom: 40px">


                          <div class ="row">
                        <div class="form-group col-md-6">
                            <label for="password" class="col-md-12 col-form-label text-md-left">{{ __('Password') }}</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="password-confirm" class="col-md-12 col-form-label text-md-left">{{ __('Confirm Password') }}</label>

                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                    </div>

                   

                        <div class="form-group row mb-0">
                            <div class="form-submit">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                                
                            </div>
                            
                        </div>

         
                               

 
               
            </div>    
                
            
        </div>
</div>
 </form> 
    
    </div>
    </div>
    </div>
    @stop
