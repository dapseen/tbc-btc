@extends('layouts.app') @section('content')

<div class="container-fluid dashboard-container">

    <div class="row">

        @include('sidebar')

        <div class="col-md-10 dashboard-body">

            <div class="row">

                <div class="col-md-1">

                </div>

                <div class="col-md-10">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="section-heading" style="margin: 5px 15px">Card Charge</div>
                            <div class="dashboard-content-section" style="height: auto; padding: 20px 30px; margin: 15px 15px 40px">

                                @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                                @endif

                                <form method="POST" action="" id="bank-form" name="wallet-form">
                                    @csrf
                                    
                                    <div class="row">

                                        <div class="col-md-6">

                                            <div>

                                                <label for="Name" class="col-form-label text-md-left">First Name</label>
                                                <input id="name" type="text" class="form-control" name="first_name" value="{{old('cypto_wallet_address')}}" required> @if ($errors->has('cypto_wallet_address'))
                                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('cypto_wallet_address') }}</strong>
                                            </span> @endif

                                            </div>

                                        </div>

                                    </div>

									
                                    <div class="row">

                                        <div class="col-md-6">

                                            <div>

                                                <label for="Name" class="col-form-label text-md-left">Last Name</label>
                                                <input id="name" type="text" class="form-control" name="last_name" value="{{old('cypto_wallet_address')}}" required> @if ($errors->has('cypto_wallet_address'))
                                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('cypto_wallet_address') }}</strong>
                                            </span> @endif

                                            </div>

                                        </div>

                                    </div>
									
                                    <div class="row">

                                        <div class="col-md-6">

                                            <div>

                                                <label for="Name" class="col-form-label text-md-left">Card Number</label>
                                                <input id="name" type="text" class="form-control" name="card_number" value="{{old('cypto_wallet_address')}}" required> @if ($errors->has('cypto_wallet_address'))
                                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('cypto_wallet_address') }}</strong>
                                            </span> @endif

                                            </div>

                                        </div>

                                    </div>
									
									
                                    <div class="row">

                                        <div class="col-md-6">

                                            <div>

                                                <label for="Name" class="col-form-label text-md-left">CVV</label>
                                                <input id="name" type="text" class="form-control" name="cvv" value="{{old('cypto_wallet_address')}}" required> @if ($errors->has('cypto_wallet_address'))
                                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('cypto_wallet_address') }}</strong>
                                            </span> @endif

                                            </div>

                                        </div>

                                    </div>
									
									
                                    <div class="row">

                                        <div class="col-md-6">

                                            <div>

                                                <label for="Name" class="col-form-label text-md-left">Expiry</label>
                                                <input id="name" type="text" class="form-control" name="expiry" value="{{old('cypto_wallet_address')}}" required> @if ($errors->has('cypto_wallet_address'))
                                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('cypto_wallet_address') }}</strong>
                                            </span> @endif

                                            </div>

                                        </div>

                                    </div>
									
									
                                    <div class="row">

                                        <div class="col-md-6">

                                            <label for="amount" class=" col-form-label text-md-left">Amount</label>

                                            <div class="">
                                                <input id="amount" type="number" step="0.1" min="0" class="form-control{{ $errors->has('requested_amount') ? ' is-invalid' : '' }}" name="requested_amount" value="{{ old('requested_amount') }}" required autofocus> @if ($errors->has('requested_amount'))
                                                <span class="invalid-feedback">
                                                <strong>{{ ($errors->first('requested_amount') ) }}</strong>
                                                </span> @endif
                                            </div>

                                        </div>

                                    </div>

                                    <div class="row">

                                        <div style="padding-left: 15px">
                                            <button style="margin: 10px 0px 20px" type="submit" class="btn btn-primary">
                                                {{ __('Charge') }}
                                            </button>
                                        </div>

                                    </div>

                                </form>

                            </div>

                        </div>

                    </div>

                    <div class="col-md-1">

                    </div>

                </div>

            </div>

        </div>

    </div>
    @endsection