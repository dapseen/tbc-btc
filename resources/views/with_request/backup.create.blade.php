@extends('layouts.app') @section('content')

<div class="container-fluid dashboard-container">

    <div class="row">

        @include('sidebar')

        <div class="col-md-10 dashboard-body">

            <div class="row">

                <div class="col-md-1">

                </div>

                <div class="col-md-10">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="section-heading" style="margin: 5px 15px">Withdrawal Request</div>
                            <div class="dashboard-content-section" style="height: auto; padding: 20px 30px; margin: 15px 15px 40px">

                                @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                                @endif

                                <div id="helper-trans">
                                    <h6 style="text-decoration: none; color: #666">Choose withdrawal type:</h6>
                                    
                                    <select class="form-control select-withdrawal" id ="">
                                        @php //<option value ="Bank Account">Bank Account</option> @endphp
                                        <option value ="Crypto Account">Cryptocurrency Wallet</option>
                                        
                                    </select>
                                    <div style="display: flex;">
                                    
                                    </div>
                                </div>
                               
                                @php /*<form method="POST" action="" id="bank-form" name="bank-form">
                                    @csrf
                                    <div class="row">

                                        <div class="col-md-12">

                                            <div class="form-group">

                                                <label for="bank_account_name" class=" col-form-label text-md-left">Bank Account Name</label>

                                                <select id="bank_account_name" class="form-control{{ $errors->has('bank_account_name') ? ' is-invalid' : '' }}" name="bank_account_name" value="{{ old('bank_account_name') }}" required>
                                                    <option value="access_bank">Access Bank</option>
                                                    <option value="citi_bank">Citi Bank</option>
                                                    <option value="diamond_bank">Diamond</option>
                                                    <option value="fidelity_bank">Fidelity</option>
                                                    <option value="first_bank">First Bank</option>
                                                    <option value="Diamond">Diamond</option>
                                                    <option value="gtbank">GT Bank</option>
                                                    <option value="skyebank">Skye Bank</option>
                                                    <option value="stanbic">Stanbic IBTC</option>
                                                    <option value="uba">UBA</option>
                                                    <option value="zenith">Zenith</option>
                                                </select>

                                                @if ($errors->has('bank_account_name'))
                                                <span class="invalid-feedback">
                                                <strong></strong>
                                                 </span> @endif

                                            </div>

                                        </div>

                                    </div>
                                    <div class="row">

                                        <div class="col-md-12">

                                            <div>

                                                <label for="Name" class="col-md-12 col-form-label text-md-left">Account Holder Name</label>
                                                <input id="name" type="text" class="form-control" name="bank_account_holder_name" value="{{old('bank_account_holder_name')}}" required> @if ($errors->has('bank_account_holder_name'))
                                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('bank_account_holder_name') }}</strong>
                                            </span> @endif

                                            </div>

                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-md-6">

                                            <div>

                                                <label for="account_number" class="col-md-12 col-form-label text-md-left">Account Number</label>
                                                <input id="name" type="number" class="form-control" name="bank_account_number" value="{{ Auth::user()->account_number}}" required readonly> @if ($errors->has('bank_account_number'))
                                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('bank_account_number') }}</strong>
                                            </span> @endif

                                            </div>

                                        </div>
                                        <div class="col-md-6">

                                            <div class="">

                                                <label for="amount" class="col-md-12 col-form-label text-md-left">Amount</label>

                                                <div class="">
                                                    <input id="amount" type="number" step="0.1" min="0" class="form-control{{ $errors->has('requested_amount') ? ' is-invalid' : '' }}" name="requested_amount" value="{{ old('requested_amount') }}" required autofocus> @if ($errors->has('requested_amount'))
                                                    <span class="invalid-feedback">
                                                <strong>{{ ($errors->first('requested_amount') ) }}</strong>
                                                </span> @endif
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="row">

                                        <input type="hidden" name="from_investment_id" value="{{$investment_id}}" />
                                        <input type="hidden" name="withdrawal_method" value="bank" />
                                        <input type="hidden" name="currency" value="local" />

                                        <div style="padding-left: 15px">
                                            <h6 style="text-decoration: underline; color: #666">Charge fee: 1.5% </h6>
                                            <button style="margin: 10px 0px 20px" type="submit" class="btn btn-primary">
                                                {{ __('Request Withdrawal') }}
                                            </button>
                                        </div>

                                    </div>

                                </form> */ @endphp

                                <form method="POST" action="" id="bank-form" name="wallet-form">
                                    @csrf
                                    <div class="row">

                                        <div class="col-md-12">

                                            <div class="form-group">

                                                <label for="bank_account_name" class=" col-form-label text-md-left">Currency</label>

                                                <select id="currency" class="form-control{{ $errors->has('currency') ? ' is-invalid' : '' }}" name="currency" value="{{ old('currency') }}" required>
                                                    <option value="BTC">Bitcoin</option>
                                                    <option value="ETH">Ethereum</option>
                                                    <option value="LTC">Litecoin</option>
                                                </select>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="row">

                                        <div class="col-md-12">

                                            <div>

                                                <label for="Name" class="col-form-label text-md-left">Your Wallet Address</label>
                                                <input id="name" type="text" class="form-control" name="cypto_wallet_address" value="{{old('cypto_wallet_address')}}" required> @if ($errors->has('cypto_wallet_address'))
                                                <span class="invalid-feedback">
                                            <strong>{{ $errors->first('cypto_wallet_address') }}</strong>
                                            </span> @endif

                                            </div>

                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-md-6">

                                            <label for="amount" class=" col-form-label text-md-left">Amount</label>

                                            <div class="">
                                                <input id="amount" type="number" step="0.1" min="0" class="form-control{{ $errors->has('requested_amount') ? ' is-invalid' : '' }}" name="requested_amount" value="{{ old('requested_amount') }}" required autofocus> @if ($errors->has('requested_amount'))
                                                <span class="invalid-feedback">
                                                <strong>{{ ($errors->first('requested_amount') ) }}</strong>
                                                </span> @endif
                                            </div>

                                        </div>

                                    </div>

                                    <div class="row">

                                        <input type="hidden" name="from_investment_id" value="{{$investment_id}}" />
                                        <input type="hidden" name="withdrawal_method" value="wallet" />

                                        <div style="padding-left: 15px">
                                            <h6 style="text-decoration: underline; color: #666">Charge fee: 1.5% </h6>
                                            <button style="margin: 10px 0px 20px" type="submit" class="btn btn-primary">
                                                {{ __('Request Withdrawal') }}
                                            </button>
                                        </div>

                                    </div>

                                </form>

                            </div>

                        </div>

                    </div>

                    <div class="col-md-1">

                    </div>

                </div>

            </div>

        </div>

    </div>
    @endsection