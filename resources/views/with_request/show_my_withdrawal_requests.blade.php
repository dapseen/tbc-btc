@extends('layouts.app')

@section('content')

<div class="container-fluid dashboard-container">

    @if (session('status'))
        <div class="alert alert-success" align="center">
            {{ session('status') }}
        </div>
    @endif
	
    <div class ="row">

        @include('sidebar')

         <div class ="col-md-10 dashboard-body">

                <div class ="row">

                         <div class ="col-md-1">


                         </div>

                         <div class ="col-md-10">
                              
                           <div class ="row"> 
                              
                              <div class ="col-md-12">

                                  <div class ="dashboard-content-section" style ="height: auto; padding: 0px">

                                    <table class="table">
                                   
                                     <thead>
                                       <tr>
                                          <th>Bank Name</th>
                                          <th>Bank Account Holder Name</th>
                                          <th>Bank Account Number</th>-
                                          <th>Wallet Address</th>
                                          <th>Currency</th>
                                          <th>Requested Amount</th>
                                          <th>Status</th>
                                          <th></th>
                                       </tr>
                                     </thead>
                                     <tbody>
                                        @foreach ($requests as $withdrawalRequest)
										<tr>
                                           <td>{{ $withdrawalRequest->bank_account_name }}</td>
                                           <td>{{ $withdrawalRequest->bank_account_holder_name }}</td>
                                           <td>{{ $withdrawalRequest->bank_account_number }}</td>
                                           <td>{{ $withdrawalRequest->cypto_wallet_address }}</td>
                                           <td>{{ $withdrawalRequest->currency }}</td>
                                           <td>{{ $withdrawalRequest->requested_amount }} USD</td>
                                           <td>{{ $withdrawalRequest->request_status }}</td>
										   </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                    </div>
                               </div>
                               

                            </div>

                            
							
                        </div>
						 
						 <div class ="col-md-1">


                         </div>

                </div>




         </div>









    </div>
    
						 
                         
</div>
@endsection
