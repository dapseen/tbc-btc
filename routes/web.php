<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


//index/home-page
Route::get('/', 'SetupController@index');


//X Routes
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');


//user dashboard
Route::get('/home', 'HomeController@index');
Route::get('/home/investments/', 'MyInvestmentsController@index');
Route::get('/home/invest/{plan}', 'CreateInvestmentController@displayPaymentMethods');
Route::post('/home/invest/{plan}', 'CreateInvestmentController@register');
Route::get('/home/withdrawal/{id}', 'WithdrawalController@index');
Route::get('/home/mywithdrawal/requests', 'MyWithdrawalRequestsController@index');
Route::post('/home/withdrawal/{id}', 'WithdrawalController@register');
Route::post('/home/ref-withdrawal', 'RefWithdrawalController@register');
Route::get('/home/ref-withdrawal', 'RefWithdrawalController@index');
Route::get('/home/profile', 'ProfileController@index');
Route::get('/home/profile/{id}/edit', 'ProfileController@edit');
Route::patch('/home/profile/{id}', 'ProfileController@update');
Route::get('/home/transactions', 'TransactionsController@index');
Route::get('/home/my-referrals', 'ReferralController@my_referrals');
Route::get('/home/buy-crypto','ProfileController@buyCrypto');
Route::resource('/home/pay-with-card','PayWithCardController');

//agent form
Route::post('/home/pay-to-agent','Agent@payToAgent');

//admin dashboard
Route::get('/admin/create/', 'CreateAdminController@index');
Route::post('/admin/create/', 'CreateAdminController@register');
Route::get('/admin/users/', 'AllUsersController@index');
Route::get('/admin/transactions/', 'AllTransactionsController@index');
Route::get('/admin/investments/', 'AllInvestmentsController@index');
Route::get('/admin/admins/', 'AllAdminsController@index');
Route::get('/admin', 'AdminController@index');
Route::get('/admin/investments/activate/{id}','AllInvestmentsController@activateInvestment');
Route::get('/admin/investments/deactivate/{id}','AllInvestmentsController@deactivateInvestment');
Route::get('/admin/delete/{id}','UserDeleteController@destroy');
Route::get('/admin/users/{id}', 'AllUsersController@individual_user');
Route::get('/admin/withdrawal/requests', 'AllWithdrawalRequestsController@index');
Route::get('/admin/currency-rates/edit', 'CurrencyRatesController@index');
Route::post('/admin/currency-rates/edit', 'CurrencyRatesController@register');
Route::get('/admin/withdrawal/requests/reject/{id}', 'AllWithdrawalRequestsController@rejectRequest');
Route::get('/admin/withdrawal/requests/accept/{id}', 'AllWithdrawalRequestsController@acceptRequest');
Route::resource('/admin/bonus','AdminBonusController');

//site config
Route::get('/admin/site-config/create','SiteConfigController@index');
Route::post('/admin/site-config/create','SiteConfigController@create');